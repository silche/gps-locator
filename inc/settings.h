#ifndef SETS_H_INC
#define SETS_H_INC

#include "custom_types.h"

#define APP_DEBUG

#define MAX_IMEI_LEN 15
#define TEL_NUMBER_LENGTH 11
#define MAX_DOMAIN_NAME_LEN 32

typedef enum
{
    ADDRESS_TYPE_IP,
    ADDRESS_TYPE_DOMAIN
}ADDRESS_TYPE;

#pragma anon_unions
typedef struct
{
    //Server configuration
    ADDRESS_TYPE address_type;
    union
    {
        char domain[MAX_DOMAIN_NAME_LEN];
        u8 IPaddress[4];
    };
    u16 port;

    //������������ ������������
    u8 phone_Number[TEL_NUMBER_LENGTH+1]; //����� �������� ���������. ���� ��������� �� ��������� ������ �� ����� ������
    u8 sms_Password[5]; //������ - 4 �������, � ������� �������� ����������� ��������� ������ �� SMS ���������

    u8 device_name[23]; //������������ ����������
    u8 balance_number[12]; //���������� ������������� USSD ����� ������� �������
    s16 utc; // �������� �������� ����� ������������ ��������

    //Timer configuration
    struct
    {
        u32 sleep_timer_period;
        u32 timeupdate_timer_peroid;
        u32 gps_send_period;
        u32 main_app_timer_period;
    };

}SETTINGS;

extern SETTINGS setts;

/* Example of a stored structure
{
    "USER":
    {
        "PHONE_NUMBER":"7986543210",
        "PASSWORD":"1234",
        "DEVNAME":"Tracker"
    }
}
*/
void Initial_Settings(void);
eat_bool Save_Settings(void);
eat_bool Restore_Settings(void);

#endif /* SETS_H_INC */
