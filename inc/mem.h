/*---------------------------------------------------------------------
* ������������ ����� :   mem.h
* ��������           :   ��������������� ��� ������� ��������� � ������������ ������ ������ ��� �������� ����������
*--------------------------------------------------------------------*/
#ifndef MEM_INC_H_
#define MEM_INC_H_

#include <eat_mem.h>

#define malloc eat_mem_alloc
#define free eat_mem_free

#define allocMsg(len) eat_mem_alloc(len)
#define freeMsg(msg) eat_mem_free(msg)

#endif /* MEM_INC_H_ */
