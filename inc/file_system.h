#ifndef FS_H_INCLUDED
#define FS_H_INCLUDED

#include <eat_other.h>
#include "mem.h"

#define SYSTEM_DRIVE    "C:\\"
#define TF_DRIVE        "D:\\"

#define CMD_STRING_LS   "ls"
#define CMD_STRING_RM   "rm"
#define CMD_STRING_CAT  "cat"
#define CMD_STRING_TAIL  "tail"

#define SETTINGS_FILE_PATHNAME  L"C:\\settings.cfg" //������������ �����, ��������� ��������� � �������� �������

void fs_initial(void);
SINT64 fs_getDiskFreeSize(void);

int fs_factory(void);

static eat_bool fs_get_file_size(void);
char *fs_open_read_file(void);
eat_bool fs_write_to_file(u8 content_buf[]);

#endif //FS_H_INCLUDED
