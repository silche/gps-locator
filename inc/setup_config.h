/*---------------------------------------------------------------------
* ������������ ����� :   config.h
* ���� ������        :   24.02.2017
* ��������           :   ������������ ������� �������: GPS � UART, ������, ftp-�������
*--------------------------------------------------------------------*/
#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#include "custom_types.h"

/**************UART configuration define**************/
#define eat_uart_app EAT_UART_1
#define eat_uart_at EAT_UART_NULL
#define eat_uart_debug EAT_UART_USB

/**************GPS module configuration define*********/
#define MSG_ID  0x08
#define GGA		0x01
#define GSA		0x00
#define GSV		0x00
#define GLL		0x00
#define RMC		0x01
#define VTG		0x00
#define ZDA		0x00
#define ATTR	0x01

/**************Modem configuration define*********/
#define START_FLAG (0xAA55)
#define MAX_CCID_LENGTH 20
#define MAX_IMSI_LENGTH 15

eat_bool gps_module_init(void);
eat_bool uart_initialize(void);


#endif /* CONFIG_H_INCLUDED */
