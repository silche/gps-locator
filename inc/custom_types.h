/*---------------------------------------------------------------------
* ������������ ����� :   custom_types.h
* ��������           :   ����������� ����������� ��� ����� ������
*--------------------------------------------------------------------*/
#ifndef INC_CUSTYPES_H_
#define INC_CUSTYPES_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <eat_interface.h>

/* ���� */
typedef unsigned char u8;
typedef signed char s8;
typedef signed char ascii;

typedef unsigned short int u16;
typedef signed short int s16;
typedef unsigned int u32;
typedef signed int s32;
typedef unsigned long long u64;
typedef signed long long s64;

typedef unsigned char uint8;
typedef signed char int8;
typedef unsigned short int uint16;
typedef signed short int int16;
typedef unsigned int uint32;
typedef signed int int32;

#define FALSE 0
#define TRUE 1

typedef unsigned char bool;
typedef unsigned char Boolean;

#define NULL 0

typedef void (*func_ptr) (void*);

#define ASSERT(expr) assert_internal(expr, __FILE__, __LINE__)
extern void assert_internal(bool expr, const char *file, int line);

#define DEBUG_TRACE(...) eat_trace(__VA_ARGS__)

#endif //INC_CUSTYPES_H_
