#ifndef _GEODATA_H_
#define _GEODATA_H_

#include "custom_types.h"

#define MAX_CELL_NUM 7

typedef struct
{
    u8 cmd;
    u8 length;
    u8 data[];
}MSG_THREAD;

/* CELL structure */
typedef struct
{
    short lac;       //local area code
    short cid;    //cell id
    short rxl;       //receive level
} CELL;

typedef struct
{
    short mcc;  //mobile country code
    short mnc;  //mobile network code
    char  cellNo;// cell count
    CELL cell[MAX_CELL_NUM];
} CELL_INFO;       //Cell Global Identifier

typedef struct// ��������� � ������� GPS-������ ��� ������� Wialon IPS
{
    unsigned char location_GPS_fix; //������������� ��������������, 1 - StandAlone/GPS is Fix
    float altitude; // ������
    unsigned int time; // �����
    char status_data_valid; // ��������� (������������� ������): � = ��������������, V = �������������� �������������� ��������
    double latitude; // ������
    char NS; // �����-��
    double longitude;// �������
    char EW; // �����-������
    unsigned short speed; // �������� ��� ������������ � km/�, �������������� �� �����
    float course; // ����
    unsigned int date; //����
    unsigned short satellitesNumb; // ���������� ��������� � �������
    //u8 *MagnetDeclination; // ��������� ����������
    //u8 *NEbeforeSum; // N or E
    //u8 *ControlSum; // ����������� �����
} GPS_DATA; // � ���� ���������� � ����� ��������

typedef struct
{
    eat_bool isGps;    //TRUE: GPS; FALSE: CELL
        GPS_DATA gpsData;
        CELL_INFO cellInfo;
}LOCATION;

//#pragma anon_unions//������������� ��������� �����-�� �� �����, ������ ���������� ������� ���.
//��������� ����������� �������� �� union ��� �����, ����� ��� ����� �������, ���� ���� ������� �����������
typedef struct
{
        GPS_DATA gpsData;        //GPS
        struct          //CELL
        {
            short mcc;
            short mnc;
            short lac;
            short cid;
        };

} LOCATION_INFO_MSG;

typedef struct
{
    short year;
    short mon;
    short day;
    short hour;
    short min;
    short sec;
} TIME;

//#pragma pack(push, 1)
typedef struct
{
    float hdop;
    char satellites;
    int managerSeq;
}__attribute__((__packed__))GPS_HDOP_INFO;

typedef struct
{
    int managerSeq;
}__attribute__((__packed__))MANAGERSEQ_INFO;
//#pragma pack(pop)

int gps_size(void);

LOCATION *get_last_gps(void);
int gps_save_last(LOCATION *gps);

void set_manager_ATcmd_state(char state);
eat_bool get_manager_ATcmd_state(void);

void set_manager_seq(int seq);
int get_manager_seq(void);

#define MAX_GPS_COUNT 10

#endif //_GEODATA_H_
