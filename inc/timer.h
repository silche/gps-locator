/*---------------------------------------------------------------------
* ������������ ����� :   timer.h
* ��������           :   ���������������� �������, �� ��������� ������� ���������� ������� ��������� �������
*--------------------------------------------------------------------*/
#ifndef INC_TIMER_H_
#define INC_TIMER_H_

#include <eat_timer.h>

#define TIMER_MAIN_LOOP     EAT_TIMER_1     //�������� ���� ��������� ������� ������
#define TIMER_GPS  	        EAT_TIMER_2     //������ ��������� ���������� GPS-������ � ������ ��������� app_gps
#define TIMER_GPS_SEND      EAT_TIMER_3     //������ �������� ��������� �� ��������-������ � �������� ������
#define TIMER_MSG_RESEND    EAT_TIMER_4     //��������� �������� ���������
#define TIMER_UPDATE_RTC    EAT_TIMER_5     //������ ���������� ���������� ����� RTC
#define TIMER_VOLTAGE_GET   EAT_TIMER_6     //��������� ���������� �������
#define TIMER_BATTERY_CHECK EAT_TIMER_7     //�������� ��������� �������

//#define TIMER_SEEKAUTOOFF   EAT_TIMER_8
//#define TIMER_VIBRATION     EAT_TIMER_9
//#define TIMER_MOVE_ALARM    EAT_TIMER_10

#endif /* INC_TIMER_H_ */
