#ifndef INC_MODEM_H_
#define INC_MODEM_H_

#include <eat_modem.h>

eat_bool modem_IsCallReady(char *modem_rsp);
eat_bool modem_IsSMSReady(char *modem_rsp);
char *modem_IsUSSDrequ(char *modem_rsp);

#if 0
eat_bool modem_ReadGPRSStatus(void);
eat_bool modem_IsGPRSAttached(char *modem_rsp);
#else
eat_bool modem_GPRSAttach(void);
#endif

eat_bool modem_switchEngineeringMode(int mode, int Ncell);

eat_bool modem_readCellInfo(void);
eat_bool modem_readCCIDInfo(void);
eat_bool modem_IsCCIDOK(char *modem_rsp);

eat_bool modem_Callphone_number(unsigned char *phone_number);
eat_bool modem_reqUSSD_number(const char *ussd_number);
eat_bool modem_cancelUSSD_session(void);

eat_bool modem_AT(unsigned char *cmd);

#endif /* INC_MODEM_H_ */
