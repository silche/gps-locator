/*---------------------------------------------------------------------
* ������������ �����   :   gps_tools.c
* ��������             :   ����������� ������������ GPS �������
* ���� ������          :   03.03.2017
*---------------------------------------------------------------------*/

#include "custom_types.h"

#include "gps_tools.h"
#include "utilities.h"

//������ ������������ ����������, ��������������� ��������������, � ������, ������������ �� ��������-������ ������� �����������
unsigned char *assembly2Wialon(void)
{
    LOCATION *gps_local = get_last_gps();
    //����� (������ ��������) � GPS ������� � ������� Wialon IPS ��� �������� �� ������
    u8 str_for_server[GPS_WIALON_BUF_LEN] = { 0 };
    u8 longitude[10] = { 0 };

    if(gps_local->isGps == TRUE)    //�������������� ����������� �� ��������� GPS?
    {
        //������ � �������� ����� ������� Wialon ����������� ����������
        //Wialon IPS: #SD#date;time;lat1;lat2;lon1;lon2;speed;course;height;sats\r\n

        if((gps_local->gpsData.longitude) < 10000)
            sprintf(longitude,"0%.4lf",gps_local->gpsData.longitude);
        else
            sprintf(longitude,"%.4lf",gps_local->gpsData.longitude);

        //� ��������� ���������� ������� ������ ���������� ����� ����� %.0f
        sprintf(str_for_server, "#SD#NA;NA;%.4lf;%c;%s;%c;%d;%.0f;%.0f;%d\r\n",
                gps_local->gpsData.latitude,gps_local->gpsData.NS, longitude, gps_local->gpsData.EW,
                gps_local->gpsData.speed, gps_local->gpsData.course,
                gps_local->gpsData.altitude, gps_local->gpsData.satellitesNumb);

        DEBUG_TRACE("gps_wialonbuf: %s", str_for_server);
    }
    return str_for_server;
}

//������ ��������� ����� ������ � ������� ������������
long double getdistance(LOCATION *pre_gps, LOCATION *gps)
{
    long double radLat1 = DEG2RAD(pre_gps->gpsData.latitude);
    long double radLat2 = DEG2RAD(gps->gpsData.latitude);
    long double a = radLat1 - radLat2;
    long double b = DEG2RAD(pre_gps->gpsData.longitude) - DEG2RAD(gps->gpsData.longitude);

    long double s = 2 * asin(sqrt(sin(a/2)*sin(a/2)+cos(radLat1)*cos(radLat2)*sin(b/2)*sin(b/2)));
    DEBUG_TRACE("Lat1:%lf,Lat2:%lf,Lon1:%lf,Lon2:%lf",pre_gps->gpsData.latitude,gps->gpsData.latitude,
              pre_gps->gpsData.longitude,gps->gpsData.longitude);

    s = s * EARTH_RADIUS;
    return s ;
}

//���������� ������ �� ��������������
void update_location_data(LOCATION *locate_Data)
{
    //LOCATION *last_gps = get_last_gps();
    DEBUG_TRACE("CELL data: mcc=%d,_mnc=%d,lac=%x,cellid=%x",
                locate_Data->cellInfo.mcc, locate_Data->cellInfo.mnc, locate_Data->cellInfo.cell[0].lac,
            locate_Data->cellInfo.cell[0].cid);
    //memcpy(last_gps, locate_Data, sizeof(LOCATION));
    gps_save_last(locate_Data);//���������� ��������� GPS ������
    DEBUG_TRACE("��������� ��������� � �������!\n");
    return;
}
