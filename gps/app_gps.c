/*---------------------------------------------------------------------
* ������������ ����� :   gps.�
* ���� ������        :   16.05.2017
* ��������           :   ��������� � ���������� ��������� �� ����� ������� NMEA � ���������� �� � ��������� GPS-������
*---------------------------------------------------------------------*/
/* ������������ ����� */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "custom_types.h"
#include <eat_modem.h>

#include "timer.h"
#include "uart.h"
#include "settings.h"
#include "modem.h"
#include "geodata.h"
#include "utilities.h"
#include "gps_tools.h"

#include "thread.h"
#include <nmea/gmath.h>

/* ������� */
#define TIMER_GET_GPS_PERIOD (5 * 1000)
#define TIMER_GET_CELL_PERIOD (60 * 1000)
#define READ_BUFF_SIZE 2048

/* ��������� ���������� (STATIC) */
//static - ����� ������ � ���� C-�����
static eat_bool gps_GetGps(void);
static void Get_location_data(void);
static void gps_at_read_handler(void);
static eat_bool parseNmeaData(u8*);

static eat_bool request_get_CellInfo = FALSE; //������ �� ��������� ������ ������������ ������� �������

static eat_bool isGpsFixed = FALSE; //�������� �� ������ �� ��������� ������� GPS
static eat_bool isCellGet = FALSE; //�������� �� ������ � ������� �������� ������� ����

static LOCATION loc_Data; //��������� � ����������� � ��������������

static float speed_knots = 0.0; //�������� � ����� ��� �������� � ��/�
static float time_ms = 0.0; //����� � ��������������

/* ��������� ������� */
void app_gps_thread(void *data)
{
    EatEvent_st user_event;

    DEBUG_TRACE("App GPS entry!");

    //������� ������ ������ � ���������� �����, ��� ��������� ���������� � ����. �������: AT+CENG=3,1\r
    modem_switchEngineeringMode(3, 1);   request_get_CellInfo = TRUE;

    eat_timer_start(TIMER_GPS, TIMER_GET_GPS_PERIOD);

    while(TRUE)
    {
        eat_get_event_for_user(THREAD_GPS, &user_event);
        if (user_event.event == EAT_EVENT_NULL || user_event.event >= EAT_EVENT_NUM)
            //DEBUG_TRACE("[%s] invalid user_event type", __FUNCTION__, user_event.event);
            continue;
        else
            DEBUG_TRACE("GPS: [%s] user_event_id=%d", __FUNCTION__, user_event.event);

        switch(user_event.event)
        {
        case EAT_EVENT_TIMER :
            switch (user_event.data.timer.timer_id)
            {
            case TIMER_GPS:
                DEBUG_TRACE("TIMER_GPS expire."); //����
                Get_location_data(); //��������� � ���������� ������ � ��������������
                if(isGpsFixed == TRUE)
                    eat_timer_start(TIMER_GPS, TIMER_GET_GPS_PERIOD);
                else
                {
                    DEBUG_TRACE("GPS is Unfix.");
                    DEBUG_TRACE("Get CELL location.");
                    eat_timer_start(TIMER_GPS, TIMER_GET_CELL_PERIOD);
                    //������ ������ � ������������ � ������ ������� �������� ������� ����
                    modem_readCellInfo();   //AT+CENG?\r
                }
                break;
            default:
                DEBUG_TRACE("timer[%d] ����!", user_event.data.timer.timer_id);
                break;
            }
            break;

        case EAT_EVENT_MDM_READY_RD:
            if(isGpsFixed == FALSE)
            {
                DEBUG_TRACE("�������� AT ������� �� ������ ������ � ������� ������.");
                gps_at_read_handler();
                request_get_CellInfo = FALSE;
            }
            break;

        default:
            DEBUG_TRACE("user_event %d not processed!", user_event.event);
            break;
        }
    }
}

//��������� ������ � ��������������
static void Get_location_data(void)
{
    if(gps_GetGps() || isCellGet) //�������� �� ������ � �������������� �� ��������� ������� GPS
    {//��� �������� �� ����� ���������� � ������� ��������
        loc_Data.isGps = isGpsFixed;
        update_location_data(&loc_Data); //���������� ����������� ������ � ���������
    }
}

static eat_bool gps_GetGps(void)
{
    u8 *uart_buf = get_uart_Buf();
    isGpsFixed = FALSE;
    DEBUG_TRACE("UART buffer: %s", uart_buf);

    if(strlen(uart_buf) > 0)
    {
        isGpsFixed = parseNmeaData(uart_buf); // �������� GPS ������ � ������� NMEA
    }
    return isGpsFixed;
}

static eat_bool parseNmeaData(u8 *uart_buf)
{
    u8 *nmea_msg_buf = {0};
    memset(&loc_Data.gpsData, 0, sizeof(loc_Data.gpsData)); // ������� ���������

    //�������� ������ � ������������ NMEA ��������� (���������)
    //� ��������� ���������� ������ �� ������������

    nmea_msg_buf = string_bypass(uart_buf, "$GPGGA,"); //����� GGA ���������

    //������ ������ �� ������ � ���������� ���������� ������������, �� ���������� ������������ ���� (%f, %d...)
    sscanf(nmea_msg_buf,"%*10s,%*f,%*c,%*f,%*c,%hhu,%d,%*f,%f,%*c,%*f,%*c,%*f,%d*",
           &loc_Data.gpsData.location_GPS_fix, &loc_Data.gpsData.satellitesNumb, &loc_Data.gpsData.altitude);
    //&latitude,&longtitude,&satellites,&hdop);

    //$GPRMC,	Hhmmss.ss,	A,	1111.11,	A,	yyyyy.yy,	a,	x.x ,	x.x,	ddmmyy,	x.x,	A	*hh	<CR><LF>
    nmea_msg_buf = string_bypass(uart_buf, "$GPRMC,");
    sscanf(nmea_msg_buf,"%f,%c,%lf,%c,%lf,%c,%f,%f,%d",
           &time_ms, &loc_Data.gpsData.status_data_valid, &loc_Data.gpsData.latitude,
           &loc_Data.gpsData.NS, &loc_Data.gpsData.longitude,  &loc_Data.gpsData.EW,
           &speed_knots,  &loc_Data.gpsData.course, &loc_Data.gpsData.date);

    loc_Data.gpsData.time = (unsigned int) time_ms;
    //�������������� ��������, ���������� � �����, � ��/�
    loc_Data.gpsData.speed = (unsigned short int)(speed_knots * ONE_KNOT);

    DEBUG_TRACE("������������� ��������������: %hhu", loc_Data.gpsData.location_GPS_fix);
    DEBUG_TRACE("������������� ������: %c", loc_Data.gpsData.status_data_valid);

    //�������� �������� �� ��������� ������
    if (loc_Data.gpsData.location_GPS_fix == 1 && loc_Data.gpsData.status_data_valid == 'A'
            && loc_Data.gpsData.latitude > 0  && loc_Data.gpsData.longitude > 0)
    {
        DEBUG_TRACE("�������������� �����������!");
        return TRUE;
    }
    else
        return FALSE;
}

static void gps_at_read_handler(void)
{
    unsigned char *buf_p1 = NULL;
    unsigned char *buf_p2 = NULL;
    unsigned char  buf[READ_BUFF_SIZE] = {0};
    unsigned int len = 0;
    unsigned int count = 0, cellCount = 0;
    int _mcc = 0;
    int _mnc = 0;
    int lac = 0;
    int cellid = 0;
    int rxl = 0;

    len = eat_modem_read(buf, READ_BUFF_SIZE);
    //DEBUG_TRACE("modem read, len=%d, buf=%s", len, buf);

    buf_p1 = string_bypass(buf, "AT+CENG=3,1\r\r\n");
    if(NULL != buf_p1)
    {
        buf_p2 = (unsigned char*)strstr(buf_p1, "OK");
        if(buf_p1 == buf_p2)
        {
            DEBUG_TRACE("turn on cells OK.");
        }
    }
    buf_p1 = string_bypass(buf, "+CENG: 3,1\r\n\r\n");
    if(NULL != buf_p1)
    {
        while(buf_p1)
        {
            buf_p1 = string_bypass(buf_p1, "+CENG: ");

            count = sscanf(buf_p1, "%d,\"%d,%d,%x,%x,%*d,%d\"", &cellCount, &_mcc, &_mnc, &lac, &cellid, &rxl);
            //DEBUG_TRACE("CELL info: cellCount=%d,_mcc=%d,_mnc=%d,lac=%x,cellid=%x,rxl=%d",
            //            cellCount, _mcc, _mnc, lac, cellid, rxl);
            if(6 != count)
            {
                continue;
            }

            if(0 == loc_Data.cellInfo.mcc)
            {
                loc_Data.cellInfo.mcc = _mcc;
                loc_Data.cellInfo.mnc = _mnc;
            }
            loc_Data.cellInfo.cell[cellCount].lac = lac;
            loc_Data.cellInfo.cell[cellCount].cid = cellid & 0xffff;
            loc_Data.cellInfo.cell[cellCount].rxl = rxl;
        }

        if(0 != loc_Data.cellInfo.mcc)
        {
            isCellGet = TRUE;
            loc_Data.cellInfo.cellNo = cellCount + 1;//7
            DEBUG_TRACE("������ � ������� �������� ������� ���� ��������!");
        }
        else
            isCellGet = FALSE;

        DEBUG_TRACE("CELL: mcc=%d,_mnc=%d,lac=%x,cellid=%x,rxl=%d",
                    loc_Data.cellInfo.mcc, loc_Data.cellInfo.mnc, loc_Data.cellInfo.cell[0].lac,
                loc_Data.cellInfo.cell[0].cid, loc_Data.cellInfo.cell[0].rxl);
        DEBUG_TRACE("Cell get: %d", isCellGet);
    }
    return;
}
