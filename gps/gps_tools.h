#ifndef GPS_TOOLS_H_
#define GPS_TOOLS_H_

#include "geodata.h"

#define GPS_WIALON_BUF_LEN 2048 //������ ������ ������ � ������� Wialon
#define ONE_KNOT 1.852000012 //�������� �������� ������ ���� � ��/� ��� ����������� �������� � ��/�
#define PI 3.141592653 //����� ��
#define DEG2RAD(d) (d * PI / 180.f)//������� � �������
#define EARTH_RADIUS 6378137 //������ ����� � �������� ��������� - �����

unsigned char *assembly2Wialon(void);
void update_location_data(LOCATION *locate_Data);
eat_bool gps_DuplicateCheck(LOCATION *pre_gps, LOCATION *gps);
long double getdistance(LOCATION *pre_gps, LOCATION *gps);

#endif //GPS_TOOLS_H_
