/*---------------------------------------------------------------------
* ������������ ����� :   uart.c
* ��������           :   ��������� ������ �� GPS-������ �� ���������� ���� (������������� ����������� ���������������)
*---------------------------------------------------------------------*/

/* ������������ ����� */
#include "uart.h"
#include "setup_config.h"

/* ��������� ���������� (STATIC) */
static u8 UART_Buffer[EAT_UART_RX_BUF_LEN_MAX + 1] = { 0 }; //���������� ������, � ������� ����������� ������ �� ����
static u8 *uart_Buf = UART_Buffer;
static u16 wr_uart_offset = 0; //�������� ������ ��� ������ � ����
static u16 wr_uart_len = 0; //����� ������ � ����

/* ��������� ������� */
//������� ����� ������� � ������, ��������� �� ���� �����
u8 *get_uart_Buf(void)
{
    return uart_Buf;
}

//��������� ������ ����� ������������� ����������� ��������������� (����)
int event_uart_rx_proc(const EatEvent_st *event)
{
    u16 length = 0; //����� ����������� �������
    EatUart_enum uart = event->data.uart.uart; //��������� ������ �� ������������� �������
    length = eat_uart_read(uart, UART_Buffer, EAT_UART_RX_BUF_LEN_MAX); //������ ���������� ������ �����

    if (length != 0) //���� ������ ��������
    {
        UART_Buffer[length] = '\0';//���������� ������� �������, ���������� ����� ������
        //TRACE_DEBUG("[%s] UART(%d) received: %s", __FUNCTION__, uart, UART_Buffer);
    }
    return 0;
}

UART_WRITER *uart_writer = 0;
void uart_setWrite(UART_WRITER writer)
{
    uart_writer = writer;
}
//�������� ������ �� ����
int event_uart_wr_proc(const EatEvent_st *event)
{
    u16 len;

    if (uart_writer)
        uart_writer();
    else
        TRACE_DEBUG("uart wr event not handled");

#if defined(EAT_AT_TEST)
    len = eat_uart_write(eat_uart_app, &UART_Buffer[wr_uart_offset], wr_uart_len);
    if (len < wr_uart_len)
    {
        wr_uart_offset += len;
        wr_uart_len -= len;
        return;
    }
    do
    {
        len = eat_modem_read(UART_Buffer, EAT_UART_RX_BUF_LEN_MAX);
        if (len>0)
        {
            UART_Buffer[len] = '\0';
            //TRACE_DEBUG("[%s] uart(%d) rx from modem: %s", __FUNCTION__, eat_uart_app, rx_buf);
            wr_uart_offset = eat_uart_write(eat_uart_app, UART_Buffer, len);
            if (wr_uart_offset<len)
            {
                wr_uart_len = len - wr_uart_offset;
                break;
            }
        }
    } while (len == EAT_UART_RX_BUF_LEN_MAX);
#endif
    return 0;
}

//����� ��������� �� �������� �������� ������ �� ����
void uart_send_complete_proc(const EatEvent_st *event)
{
    TRACE_DEBUG("[%s] uart(%d) send complete!\n", __FUNCTION__, event->data.uart.uart);
}

