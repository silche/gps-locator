/*---------------------------------------------------------------------
* ������������ �����   :   app_sms.c
* ��������             :   ���� � �������� �������� ��������� ��������� �� ���������� SMS
* ���� ������          :   03.02.2017
*---------------------------------------------------------------------*/

/* ������������ ����� */
#include <stdlib.h>

#include <eat_sms.h>
#include "custom_types.h"
#include "app_sms.h"
#include "sms_requests.h"
#include "settings.h"

SIMCOM_SMS_STRUCT sms_msg;

/* ��������� ������� */
//������������� ���������� �������� (callback) ������� ���������� ���������� SMS (sms ready)
//��������� ��������� ���������� SMS ���������
static void eat_sms_ready_cb(eat_bool result)
{
    if(TRUE == result && TRUE == eat_get_sms_ready_state())
    {
        //���������� SMS ������ ��� ������, �����������
        sms_msg.formatType = PDU;
        sms_msg.mt = 1;
        eat_set_sms_format(sms_msg.formatType);//��������� ������� ���������
        eat_set_sms_cnmi(2,sms_msg.mt,0,0,0);
        DEBUG_TRACE("���������� SMS ����������������");
    }
    else
        DEBUG_TRACE("SMS Not Ready");
}

//��������� ������ SC (Service center)
eat_bool simcom_sms_sc_set(u8 *number)
{
    if(EAT_NULL == number){
        return FALSE;
    }
    DEBUG_TRACE("simcom_sms_sc_set Number=%s",number);
    return eat_set_sms_sc(number);
}

/*******************************************************************
 * SMS PROCESS FUNCTIONS
*******************************************************************/

//������ ����� ��������� (callback function)
static eat_sms_new_message_cb(EatSmsNewMessageInd_st smsNewMessage)
{
    DEBUG_TRACE("eat_sms_new_message_cb, storage=%d,index=%d",smsNewMessage.storage,smsNewMessage.index);
    simcom_sms_msg_read(smsNewMessage.index); //������ ����������� ������ ���������
}

//�������� �������, ���������� ����� �������� SMS ��������� (send sms message callback function)
static void eat_sms_send_cb(eat_bool result)
{
    result == 1 ? DEBUG_TRACE("��������� ������� ����������!") : DEBUG_TRACE("��������� �� ����������.");
}

//�������������� � �������� ��������� SMS �� ��������� �����
eat_bool sms_send(u8 *tel_numb, u8 *message_str)
{
    eat_bool result = FALSE;
    u8 **pdu_msgs = {0};
    u8 str_count = 0;
    u16 counter = 1;
    u16 dest_string_len = 0;
    u8 phone_numb[MAX_NUMBER_LENGTH+1] = {0};

    if(strcmp(tel_numb, "") && strlen(tel_numb) > 0)
        sprintf(phone_numb, "%c%s",'+',tel_numb);
    else if(strcmp(setts.phone_Number, "") && strlen(setts.phone_Number) > 0)
        sprintf(phone_numb, "%c%s",'+',setts.phone_Number);
    else
        return FALSE;

    sprintf(sms_msg.message, "%s: %s", setts.device_name, message_str);

    DEBUG_TRACE("�������� SMS: \"%s\" �� ����� %s", sms_msg.message, phone_numb);

    //�������������� ��������� �� �������� � SMS ��������� ������� PDU
    DEBUG_TRACE("Convert MSG to SMS PDU");

    pdu_msgs = sms_encode_pdu(phone_numb,sms_msg.message),sizeof(pdu_msgs);
    //memcpy(pdu_msgs,sms_encode_pdu(phone_numb,sms_msg.message),sizeof(pdu_msgs));

    str_count = atoi(pdu_msgs[0]);
    DEBUG_TRACE("���������� ��������� �� ��������: %d", str_count);

    while(counter-1 < str_count)
    {
        result = TRUE;
        DEBUG_TRACE("%d codered PDU SMS: %s", counter, pdu_msgs[counter]);
        dest_string_len = (strlen(pdu_msgs[counter])-2) / 2;
        DEBUG_TRACE("����� MSG � ������: %hu\n", dest_string_len);
        eat_send_pdu_sms(pdu_msgs[counter], dest_string_len); //�������� ��������� � PDU �������
        counter++;
    }
    return result;
}

//������ � ����������� (�������������) ������ PDU ���������
static void read_pdu_sms(u8 *data_string)
{
    SMS_PDU_STRUCT pdu_sms_struct;
    u8 *response_message = 0;

    //������������� (�������������) ���������� ��������� � ������� PDU
    sms_decode_pdu(data_string, &pdu_sms_struct);

    DEBUG_TRACE("Decoded PDU Message: %s", pdu_sms_struct.msg);
    DEBUG_TRACE("Decoded Sender Number: %s", pdu_sms_struct.telnum);
    DEBUG_TRACE("Message Length: %d\n", pdu_sms_struct.msg_length);

    //��������� ���������
    //���� SMS ��������� ������ �� ���������
    if(strlen(setts.phone_Number) != 0 && strcmp(pdu_sms_struct.telnum, setts.phone_Number))
        sms_send(setts.phone_Number, pdu_sms_struct.msg);//���������� ��������� �� ����� ������������
    else if(strlen(setts.phone_Number) == 0 || !strcmp(pdu_sms_struct.telnum,setts.phone_Number))//���� ������ ���������
        if (strcmp((response_message = sms_handler_entry(pdu_sms_struct.msg, pdu_sms_struct.telnum)), ""))
        {
            DEBUG_TRACE("�������� ��������� ���������");
            sms_send(pdu_sms_struct.telnum, response_message);
        }
}

//Callback ������� ��������� flash SMS ���������
static eat_sms_flash_message_cb(EatSmsReadCnf_st smsFlashMessage)
{
    u8 format =0;
    eat_get_sms_format(&format);
    DEBUG_TRACE("eat_sms_flash_message_cb, format=%d",format);
    if(1 == format) //����� TEXT �������
    {
        DEBUG_TRACE("eat_sms_read_cb, msg=%s",smsFlashMessage.data);
        DEBUG_TRACE("eat_sms_read_cb, datetime=%s",smsFlashMessage.datetime);
        DEBUG_TRACE("eat_sms_read_cb, name=%s",smsFlashMessage.name);
        DEBUG_TRACE("eat_sms_read_cb, status=%d",smsFlashMessage.status);
        DEBUG_TRACE("eat_sms_read_cb, len=%d",smsFlashMessage.len);
        DEBUG_TRACE("eat_sms_read_cb, number=%s",smsFlashMessage.number);

    }
    else //����� PDU
    {
        DEBUG_TRACE("Flash SMS\nSMS TYPE: PDU");
        DEBUG_TRACE("PDU Message: %s",smsFlashMessage.data);
        DEBUG_TRACE("PDU Length: %d\n", smsFlashMessage.len);
        read_pdu_sms(smsFlashMessage.data);
    }
}

static void eat_sms_read_cb(EatSmsReadCnf_st smsReadCnfContent)
{
    u8 format = 0;

    eat_get_sms_format(&format);
    DEBUG_TRACE("eat_sms_read_cb");
    if(1 == format)//TEXT mode
    {
        DEBUG_TRACE("SMS TYPE: TEXT");
        DEBUG_TRACE("Message: %s", smsReadCnfContent.data);
        DEBUG_TRACE("Datetime: %s", smsReadCnfContent.datetime);
        DEBUG_TRACE("Name: %s", smsReadCnfContent.name);
        DEBUG_TRACE("Status: %d", smsReadCnfContent.status);
        DEBUG_TRACE("Msg Length: %d", smsReadCnfContent.len);
        DEBUG_TRACE("Sender number: %s\n", smsReadCnfContent.number);
    }
    else//PDU mode
    {
        //EatSmsalPduDecode_st pdu_sms = {0};
        DEBUG_TRACE("SMS TYPE: PDU");
        DEBUG_TRACE("PDU Message: %s",smsReadCnfContent.data);
        DEBUG_TRACE("Name: %s", smsReadCnfContent.name);
        DEBUG_TRACE("Status: %d", smsReadCnfContent.status);
        DEBUG_TRACE("PDU Length: %d\n", smsReadCnfContent.len);
        /*
            eat_sms_decode_tpdu(smsReadCnfContent.data, smsReadCnfContent.len, &pdu_sms);
            DEBUG_TRACE("user_data_len: %d\n", pdu_sms.tpdu.data.deliver_tpdu.user_data_len);
            DEBUG_TRACE("user_data: %s\n", pdu_sms.tpdu.data.deliver_tpdu.user_data);
            DEBUG_TRACE("alphabet_type: %c\n", pdu_sms.tpdu.alphabet_type);
            //������������ ������������ �����
            eat_sms_orig_address_data_convert(pdu_sms.tpdu.data.deliver_tpdu.orig_addr, phoneNum);
        */
        read_pdu_sms(smsReadCnfContent.data); //����������� ������ PDU ���������
    }
}

//������������� ������� ��� ������ � SMS �����������, ����������� ������� ��������� ������
void simcom_sms_init(void)
{
    eat_set_sms_operation_mode(TRUE);
    eat_sms_register_new_message_callback(eat_sms_new_message_cb);
    eat_sms_register_flash_message_callback(eat_sms_flash_message_cb);
    eat_sms_register_send_completed_callback(eat_sms_send_cb);
    eat_sms_register_sms_ready_callback(eat_sms_ready_cb);
}

//������ ������ �������� SMS
eat_bool simcom_sms_msg_read(u16 index)
{    
    DEBUG_TRACE("simcom_sms_msg_read index = %d", index);
    DEBUG_TRACE("������ ������ SMS ���������");
    return eat_read_sms(index,eat_sms_read_cb);
}

//�������� (callback) ������� ��� �������� ���������
static void eat_sms_delete_cb(eat_bool result)
{
    DEBUG_TRACE("eat_sms_delete_cb, result=%d",result);
}

//�������� ���������
eat_bool simcom_sms_msg_delete(u16 index)
{
    DEBUG_TRACE("simcom_sms_msg_delete index = %d", index);
    return eat_delete_sms(index,eat_sms_delete_cb);
}
