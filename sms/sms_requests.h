#ifndef SMS_REQUESTS_H
#define SMS_REQUESTS_H

#include "custom_types.h"

/* ������� */
#define CMDS_COUNT 18
#define HELP_REQUESTS_COUNT 2

/* ���� */

/*
typedef struct
{
    u8 name[11];
    u8 number[11];
} HELP_REQUESTS; //��������� ������� ���������� ��� ��������� �������

typedef enum { MTS,
               MEGAFON
             } Operators;
*/

//const char * - ��������� �� ������������ ������
typedef void(*handler)(const char *);

typedef struct {

    char *string;	// �������
    handler handler; // ���������� �������

} cmd_struct; // ��������� �������

/* ���������� ��������� ������� */
//����������� ������, ���������� ��������������� ��������
void set_Telephonenumber(const char *number);
void change_Password(const char *arg);
void set_UTC(const char *arg);
void set_timer_GPS_send(const char *arg);
void set_time_Main_app(const char *arg);
void cmd_Sleep(const char *arg);
void cmd_callUser(const char *arg);
void cmd_Reset(const char *arg);
void get_GPS(const char *arg);
void get_Imei(const char *arg);
void req_Balance(const char *arg);
void reqs_USSD(const char *arg);
void set_Devname(const char *arg);
void get_Settings(const char *arg);
void set_Sleep_time(const char *arg);
void set_USSD_numbers(const char *arg);
void get_help(const char *arg);

u8 *sms_handler_entry(const char *sms_data, const char *sender_phone);
eat_bool save_new_user_phone_number(const char *sender_phone, const char *bal_numb);

/* ��������� ���������� (STATIC) */
//��������� ��������� ������
static cmd_struct commands[CMDS_COUNT] = {
    {"?,",get_help }, //�������, ������ �� ��������
    {"help,",get_help },
    {",bal",req_Balance },
    {",ussd,",reqs_USSD },
    {",setbal,",set_USSD_numbers },
    {",call",cmd_callUser },
    {",gps",get_GPS },
    {",imei",get_Imei },
    {",sets",get_Settings },
    {",settel,",set_Telephonenumber},
    {",pswd,",change_Password },
    {",devname,",set_Devname},
    {",utc,",set_UTC },
    {",gpstm,",set_timer_GPS_send },
    {",worktm,",set_time_Main_app },
    {",sleeptm,",set_Sleep_time },
    {",sleep,",cmd_Sleep },
    {",reset,",cmd_Reset }
};

#endif /* SMS_REQUESTS_H */
