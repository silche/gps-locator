//#if !defined APP_SMS_H
#ifndef APP_SMS_H
#define APP_SMS_H

#include "pdunicode.h"

typedef enum
{
    PDU,
    TEXT //������������ ������ ��������� �������, �� ������������ Unicode, ��������������, � ���������
} SMS_FORMAT; //������� SMS ���������

typedef struct
{
    SMS_FORMAT formatType; //��� SMS ���������
    u8 message[MAX_SMS_MESSAGE_SIZE]; //���������
    size_t message_length; //����� ���������
    u8 mt;      //mt = 2, flash ���������
} SIMCOM_SMS_STRUCT;

extern void simcom_sms_init(void);

eat_bool sms_send(u8 *tel_numb, u8 *message_str);
eat_bool simcom_sms_msg_read(u16 index);
eat_bool simcom_sms_sc_set(u8 *number);
eat_bool simcom_sms_msg_delete(u16 index);

#endif /* APP_SMS_H */
