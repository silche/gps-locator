/*---------------------------------------------------------------------
* ������������ �����  :   sms_requests.c
* ��������            :   ��������� ��������� �������� SMS ��������
* ���� ������         :   28.02.2017
* ��������� ����������:   27.10.2017
*---------------------------------------------------------------------*/
#include <stdio.h>

#include "sms_requests.h"
#include "settings.h"
#include "geodata.h"
#include "utilities.h"
#include <nmea/gmath.h>
#include "modem.h"
#include "mem.h"

const u8 *help_requests[HELP_REQUESTS_COUNT] = {"activ", "cmds"}; //������� ��� ��������� �������

static eat_bool need_to_save = FALSE; //������������� � ���������� ������������ ����� � �����������
static u8 text_to_send[2000] = {0}; //����� �� �������� � �������� ���������

//�������� ������ �� ����� ��� ����� ��� ����������� ������������
static u8 *check_PhoneNumb(const char *phone_number)
{
    u8 *number = (u8*)malloc(TEL_NUMBER_LENGTH + 1);
    strcpy(number, phone_number); //char* strcpy(char* str1, const char* str2);
    DEBUG_TRACE("number: %s",number);
    if(strlen(phone_number) < 10)
    {
        DEBUG_TRACE("���������� �������� ����� ������������, \"%s\" �� ������������� ������� 79876543210",
                    phone_number);
        sprintf(text_to_send, "�������� �����");
        return "";
    }
    else if(strlen(phone_number) == 11 && phone_number[0] == '7')
        return number;
    else if(strlen(phone_number) == 12 && phone_number[0] == '+')
        *number++; //������� ������ '+' � ������
    else if(strlen(phone_number) == 11 && phone_number[0] == '8')
        number[0] = '7'; // = *numb
    else if(strlen(phone_number) == 10 && phone_number[0] == '9')
    {
        //sprintf(number, "7%s", phone_number);
        number--;
        *number = '7';
    }
    else
        return "";
    DEBUG_TRACE("number: %s",number);
    return number;
}

//��������� ������ �������� ������������
void set_Telephonenumber(const char *number)
{
    u8 *numb;
    numb = check_PhoneNumb(number);
    if(strcmp(numb, ""))
    {
        strcpy(setts.phone_Number, numb);
        need_to_save = TRUE;
        sprintf(text_to_send, "Ok! ����� �����: +%s",setts.phone_Number);
        DEBUG_TRACE("����� ���������� �������� ��������� ������� ��: %s",setts.phone_Number);
    }
}

//��������� ����� ����������
void set_Devname(const char *arg)
{
    strcpy(setts.device_name, arg);
    need_to_save = TRUE;
    sprintf(text_to_send, "Ok! ������������ ����������: %s",setts.device_name);
    DEBUG_TRACE("������������ ���������� �������� ��: %s",setts.device_name);
}

//��������� �������� �����
void set_UTC(const char *arg)
{    
    setts.utc = atoi(arg); //�������������� ������ � ����� �����
    if(setts.utc > 0)
        sprintf(text_to_send, "Ok! ������� ���� = +%d",setts.utc);
    else
        sprintf(text_to_send, "Ok! ������� ���� = %d",setts.utc);
    DEBUG_TRACE("������� ���� ������� ��: %s",setts.utc);
    need_to_save = TRUE;
}

//��������� ������� �������� GPS ������ �� ������
void set_timer_GPS_send(const char *arg)
{
    //����������� �������� �������, ����� �����
    setts.gps_send_period = atoi(arg) * 1000;
    need_to_save = TRUE;
}

//��������� ������� ������ ���������� � �������� ������
void set_time_Main_app(const char *arg)
{
    //����������� �������� - ������, ����� �����
    setts.main_app_timer_period = atoi(arg)* 60 * 1000;
    need_to_save = TRUE;
}

//��������� ������ ������� � ����������
void change_Password(const char *arg)
{
    u8 *hiddenpsw = 0;
    strcpy(setts.sms_Password, arg);
    strcpy(hiddenpsw, setts.sms_Password);
    *hiddenpsw++;
    *hiddenpsw++;
    sprintf(text_to_send, "Ok! ����� ������ �������: %c*%s",*setts.sms_Password,hiddenpsw);
    DEBUG_TRACE("������ �������� ������� ��: %s",setts.sms_Password);
    need_to_save = TRUE;
}

//�������������� ������� ���������� � ����� ���
void cmd_Sleep(const char *arg)
{
    //����������� �������� - ���, ����� �����
    eat_sleep(atoi(arg) * 24 * 60 * 60 * 1000);
}

//��������� ������� ���������� ���������� � ������ ���
void set_Sleep_time(const char *arg)
{
    //����������� �������� - ���, ����� �����
    setts.sleep_timer_period = atoi(arg) * 24 * 60 * 60 * 1000;
    need_to_save = TRUE;
}

//������ ������� �� �������������
void get_help(const char *arg)
{
    u8 i = 0;
    for (i; i<HELP_REQUESTS_COUNT; i++)
        if (strcmp(arg, help_requests[i]) == 0)
            break;
    switch (i) {
    //������ ������� �� ��������� �����
    case 0:
        sprintf(text_to_send, "��� ��������� ���������� ��������� ������� \"1234\", ��� 1234 -"
                              " ����������� ������ ����������. ��� ����� �������� �������� �������������"
                              " ����� ���������� � �������� ������ ���������. ��� �������� ������� ������"
                              " �� ����� ��������� ��������� ������� \"1234,79876543210\","
                              " ��� 79876543210 - ����� \"���������\"");
/*
        sprintf(text_to_send, "����, �����, ������ � ������. ������������� � ������� ����. �� �����."
                              "������ �������.");
*/
        break;
        //������ ������� �� SMS ��������
    case 1:
        sprintf(text_to_send, "\"1234\" - ������ �������\n"
                              "\"1234\" ��� \"1234,79876543210\" - ��������� ����������\n");
        break;
    default:
        sprintf(text_to_send, "�������� �������: %s", arg);
        break;
    }
}

//������� ��������� ������������
void cmd_callUser(const char *arg)
{
    u8 *number = string_bypass(arg, ",");
    if (number != NULL)
    {
        number = check_PhoneNumb(number);
        if(strcmp(number, ""))
        {
            DEBUG_TRACE("����� �������� �� ��������� �����: %s", number);
            modem_Callphone_number(number);
        }
    }
    else
    {
        DEBUG_TRACE("����� ��������� �����");
        modem_Callphone_number(setts.phone_Number);
    }
}

//������ ������ USSD ���������
void reqs_USSD(const char *arg)
{
    if (arg != NULL)
    {
        DEBUG_TRACE("���������� USSD �������");
        modem_reqUSSD_number(arg);
    }
}

//��������� USSD ������� ������� �������
void set_USSD_numbers(const char *arg)
{
    if(arg[strlen(arg)-1] == '#')
    {
        DEBUG_TRACE("��������� ������ USSD ��� ������� ������� �� %s",  arg);
        strcpy(setts.balance_number, arg);
    }
}

//������ �� ��������� �������
void req_Balance(const char *arg)
{
    if(strlen(setts.balance_number) > 0)
        reqs_USSD(setts.balance_number);
    else
        sprintf(text_to_send, "���������� ��������� ������. �� ����� USSD ����� ������� �������. "
                              "��� ����� ������ �������� \"1234,balnum,\"");
}

//��������� ���������� � �����
void get_Imei(const char *arg)
{
    u8 imei[MAX_IMEI_LEN+1] = {0};
    DEBUG_TRACE("SMS ������ �� ��������� ���������� � �����...");

    eat_get_imei(imei, MAX_IMEI_LEN); //str(AT+CCID\r\n) = 9
    imei[MAX_IMEI_LEN]=0;
    DEBUG_TRACE("IMEI number:imei = [%s]", imei);

    //sprintf(text_to_send, "IMEI: %s %s", imei, get_Battery_props());
    sprintf(text_to_send, "IMEI: %s", imei);
    DEBUG_TRACE("���������� � �����: %s", text_to_send);// ���������� � �����
}

void get_Settings(const char *arg)
{
    DEBUG_TRACE("SMS ������ �� ��������� �������� �����...");

    //�������� ���������� �� ������� ����
    sprintf(text_to_send, "��������: %s, UTC: %d", setts.balance_number, setts.utc);
    //,settings.gps_send_period, settings.main_app_timer_period, settings.sleep_timer_period);
    DEBUG_TRACE("���������� � �����: %s", text_to_send);
}

void get_GPS(const char *arg)
{
    LOCATION *location_data = get_last_gps();
    LOCATION_INFO_MSG *loc_for_sms;
    int msgLeng = 0;
    arg = string_bypass(arg, ",");
    need_to_save = FALSE;

    DEBUG_TRACE("������ ������ �� ��������� ��������������...");
    if(location_data->isGps)
        msgLeng = sizeof(LOCATION_INFO_MSG)-4*sizeof(short);
    else
        msgLeng = sizeof(LOCATION_INFO_MSG)-sizeof(GPS_DATA)+ 4*sizeof(short);
    loc_for_sms = allocMsg(msgLeng);

    if (!loc_for_sms)
    {
        DEBUG_TRACE("Alloc defend loc_for_sms message failed!");
        //return -1;
        return;
    }

    if(location_data->isGps)    //�������������� �� GPS �����������?
    {
        loc_for_sms->gpsData.latitude = nmea_ndeg2degree(location_data->gpsData.latitude);
        loc_for_sms->gpsData.longitude = nmea_ndeg2degree(location_data->gpsData.longitude);

        if (arg != NULL)//������� �� �������������� ���������
            if(!strcmp(arg, "ext"))
            {
                //������ �� ����������� ���������� GPS
                loc_for_sms->gpsData.time = location_data->gpsData.time;
                loc_for_sms->gpsData.date = location_data->gpsData.date;
                loc_for_sms->gpsData.speed = location_data->gpsData.speed;
                loc_for_sms->gpsData.course = location_data->gpsData.course;

                sprintf(text_to_send, "%d(UTC+0),%d,%.7lf,%.7lf,Speed:%hu,Course:%.0f",
                        loc_for_sms->gpsData.time, loc_for_sms->gpsData.date,
                        loc_for_sms->gpsData.latitude, loc_for_sms->gpsData.longitude,
                        loc_for_sms->gpsData.speed, loc_for_sms->gpsData.course);
            }
            else
            {
                //https://www.google.com/maps?q=%lf,%lf
                sprintf(text_to_send, "https://maps.google.com?q=%lf,%lf",
                        loc_for_sms->gpsData.latitude, loc_for_sms->gpsData.longitude);
            }
        DEBUG_TRACE("������� ����� SMS � GPS �������: %s", text_to_send);
    }
    else
    {
        loc_for_sms->cid = location_data->cellInfo.cell[0].cid;//cell id
        loc_for_sms->mnc = location_data->cellInfo.mnc;//mobile network code - operator id
        loc_for_sms->mcc = location_data->cellInfo.mcc;//mobile country code
        loc_for_sms->lac = location_data->cellInfo.cell[0].lac;//local area code

        //�������� ���������� �� ������� ����
        sprintf (text_to_send, "GSM: mobile.maps.yandex.net/cellid_location/?cellid=%hi&operatorid=%hi&countrycode=%hi&lac=%hi",
                 loc_for_sms->cid,  loc_for_sms->mnc,
                 loc_for_sms->mcc, loc_for_sms->lac);

        DEBUG_TRACE("�������������� �� ��������� GPS �� �����������.");
        DEBUG_TRACE("�������� SMS ������ BTS GSM ������: %s", text_to_send);
    }
}

//����� ��������
void cmd_Reset(const char *arg)
{
    //����������� �������� - ���, ����� �����
    Initial_Settings();
    need_to_save = TRUE;
}

u8 *sms_handler_entry(const char *sms_data, const char *sender_phone)
{
    char i;
    char *ptr;
    char *parametr;
    //memset(text_to_send,0,sizeof(text_to_send));
    strcpy(text_to_send,"");

    // �������� ������������ ������, � ������ ������������� ������� ������

    if ( (parametr = string_bypass(sms_data, commands[0].string)) != NULL)   {
        DEBUG_TRACE("������� %s �����. ������ �������\n", sms_data);
        commands[0].handler(parametr);
    }
    else if ( (parametr = string_bypass(sms_data, commands[1].string)) != NULL)   {
         DEBUG_TRACE("������� %s �����. ������ �������\n", sms_data);
         commands[1].handler(parametr);
    }
    else {
        ptr = string_bypass(sms_data, (char*)setts.sms_Password);

        if (ptr != NULL)
        {
            DEBUG_TRACE("������ ���������.");
            if(strcmp("", setts.phone_Number) == 0) //���� ������� ������������ �� ����������
            {
                u8 tel_numb[TEL_NUMBER_LENGTH] = {0};
                u8 *ussd_bal_num = 0;
                DEBUG_TRACE("����������� ������ ������������...");
                parametr = string_bypass(ptr, ",");
                if (parametr != NULL) //������� �� �������������� ���������, ����� ��� ������ ����� �������� ��� �������� �����
                {
                    if ((ussd_bal_num = string_bypass(parametr, ",")) != NULL)
                    {
                        DEBUG_TRACE("������ ����� �������� � ����� ������� �������.");
                        if(isNumber(ussd_bal_num))
                        {
                            strcpy(tel_numb,ussd_bal_num); //���������� ����� ��������
                            strncpy(ussd_bal_num, parametr, strlen(parametr)-strlen(ussd_bal_num)-1); //���������� ����� ������� �������
                            ussd_bal_num[strlen(parametr)-strlen(ussd_bal_num) - 1] = '\0';
                        }
                        else
                        {
                            strncpy(tel_numb, parametr, strlen(parametr)-strlen(ussd_bal_num)-1); //���������� ����� ��������
                        }
                        DEBUG_TRACE("����� ��������: %s USSD ����� �������: %s", tel_numb,ussd_bal_num);

                        //���������� ������ ������������ � �������
                        save_new_user_phone_number(tel_numb, ussd_bal_num);
                    }
                    else if(isNumber(parametr))
                    {
                        DEBUG_TRACE("������ ����� ��������.");
                        save_new_user_phone_number(parametr, "");
                    }
                    else
                    {
                        sprintf (text_to_send, "�������� �������. ������� ����������� ���������� (��������� ����� ���������)");
                        DEBUG_TRACE("������ ����� �������� �� ������. ��������� ����� �����������");
                    }
                }
                else
                {
                    DEBUG_TRACE("������ ����� �������� �� ������. ��������� ����� �����������");
                    save_new_user_phone_number(sender_phone, "");
                }
            }
            else
            {
                //��������� ������� user � sender
                if (strcmp(sender_phone, setts.phone_Number) != 0)	// ���� ����������� ������
                {
                    DEBUG_TRACE("������ user � sender �� ���������.");
                    return ""; //����� �� ������
                }
                DEBUG_TRACE("������ user � sender ���������. ����������� �������.");

                for (i = 0; i < CMDS_COUNT ; i++)
                    if ( (parametr = string_bypass(ptr, commands[i].string)) != NULL )
                    {
                        DEBUG_TRACE("������� %s �����. ����������...\n", commands[i].string);
                        commands[i].handler(parametr);
                    }
                if(need_to_save)
                {
                    Save_Settings(); //���������� ����� ��������
                    need_to_save = FALSE;
                }
            }
        }
        else
            DEBUG_TRACE("������ �� ���������.");
    }
    //DEBUG_TRACE(text_to_send);
    return text_to_send;
}

//���������� ������ ������������, ��� ������ �������� ����� �������������� ������ � ����������
eat_bool save_new_user_phone_number(const char *sender_phone, const char *bal_numb)
{
    u8 *numb = 0;
    numb = check_PhoneNumb(sender_phone);
    DEBUG_TRACE("numb %s bal_numb %s", numb,bal_numb);

    if(!strcmp(numb, ""))
        return FALSE;
    else
    {
        strcpy(setts.phone_Number, numb);

        DEBUG_TRACE("�������� ����� �������� ����������!");
        DEBUG_TRACE("��� ����� ���������� ��������: %s\n",setts.phone_Number);
        if(strlen(bal_numb) > 0)
        {
            strcpy(setts.balance_number, bal_numb);
            DEBUG_TRACE("�������� �����: %s\n",setts.phone_Number);
        }

        setts.main_app_timer_period = 30 * 60 * 1000; //����� ������ ��������� ������������� ������� �� 30 �����

        // ���������� ����� ��������
        Save_Settings();

        sprintf(text_to_send, "����������� - �������! ������������: \"+%s\"", setts.device_name, setts.phone_Number);
        return TRUE;
    }
}
