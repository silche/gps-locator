/*---------------------------------------------------------------------
* ������������ �����  :   pdunicode.c
* ��������            :   ������ ��������� ���������� ��������� SMS � ������ �� � ������� PDU ���
*                         ������� ������ ���������� � �������������� ������������ ����� ��������, ������� ���������.
*                         ��� ��������:
*                         ����������� ������ � ��������� ����������� ��������� � ������ UCS2 � ������ �� � ������ PDU SMS
* ���� ������         :   03.08.2017
* ��������� ����������:   last updated 25.10.2017 (fixed encoder to ucs2)
*---------------------------------------------------------------------*/
#include <limits.h>
#include "pdunicode.h"

/*****************************************************************************
* ������� : is_bit_set
* ��������: ���������� 1, ���� ��� � ������� bitindex ����������� �������� value
*****************************************************************************/
static u8 is_bit_set(u8 value, u8 bitindex)
{
	u8 ret = value & (1 << bitindex);
	return ret;
}

/*****************************************************************************
* ������� :   hex_to_int
* ��������:   ����������� 0x21 -> 0d21
* ���������:  num - ����� � HEX ����
*****************************************************************************/
static u8 hex_to_int(u8 num)
{
	int i = 0, sum = 0;
	u8 str[2 + TERMINATE] = { 0 };
	sprintf(str, "%02x", num);

	for (i = 0; *(str + i) != '\0'; i++)
		sum = sum * 10 + (*(str + i) - 48);

	return sum;
}

/*****************************************************************************
* ������� :   bytes_array_to_hex_string
* ��������:   ����������� [0x0B,0x91,0x97,0x00,0x21,0x43,0x65,0xF7] -> "0B919700214365F7"
* ���������:  bytes - ������ ����, size - ������
* ����������: ��������������� ������ �������� buf_str
*****************************************************************************/
static u8 *bytes_array_to_hex_string(u8 *bytes, u8 size)
{
    u16 i = 0;
    u8 *buf_str = (u8*)malloc(2 * size * sizeof(u16) + TERMINATE);
    u8 *buf_ptr = buf_str;

	for (i = 0; i < size; i++)
		buf_ptr += sprintf(buf_ptr, "%02X", *(bytes + i));
    *buf_ptr = '\0';

	return buf_str;
}

/*****************************************************************************
* ������� :   reverse
* ��������:   ������������ ���������� � ������ "5F6C" -> "F5C6"
*****************************************************************************/
static void reverse(char *source)
{
	u8 i = 0;
	while (*(source + i) != '\0')
	{
		char temp = *(source + i);
		*(source + i) = *(source + i + 1);
		*(source + i + 1) = temp;
		i = i + 2;
	}
}

/*****************************************************************************
* ������� :   convert_char_to_ucs2
* ��������:   ����������� ������ � ������ (4 ������� HE: 0-9 � A-F) � ������������ � ���������� UCS2. ������: 'H' -> "0048"
*****************************************************************************/
static void convert_char_to_ucs2(u16 c, u8 *ucs2_str)
{
    if (c >= 0xFFC0 && c <= 0xFFFF) // For Letters [�-�][�-�]
        c = c - 0xC0 + 0x510;
    else if (c == 0xFFA8) // Letter �
        c = 0x401;
    else if (c == 0xFFB8) // Letter �
        c = 0x451;

    sprintf(ucs2_str + ((c < 0x80) ? 2 : 1), "%X", c);
}

/*****************************************************************************
* ������� :   decode_telnum
* ��������:   ���������� ����� PDU ������ � ����� ��������
* ���������:  num - UCS2 ������ � �������, num_sz - ������, sms - ��������� sms ���������
* ����������: ����� ��������  � ������� 79876543210
*****************************************************************************/
static u8 decode_telnum(u8 *num, size_t num_sz, SMS_PDU_STRUCT *sms)
{
	int i = 0;
	int j = 0;
    if ( (num_sz % 2) != 0 ) {
        return -1;
    }

    if ( num_sz > SMS_SENDER_SIZE ) {
        return -2;
    }

    for (i = 0, j = 0; i < num_sz; i += 2 ) {
        char n1 = num[i];
        char n2 = num[i + 1];

        sms->telnum[j] = n2;
        ++j;

        if ( n1 != 'F' ) {
            sms->telnum[j] = n1;
            ++j;
        }
    }

    return 0;
}

/*****************************************************************************
* FUNCTION
*  char_to_u8
* DESCRIPTION
* '5' -> 0d05, 'A' -> 0d10
*****************************************************************************/
static int char_to_u8(u8 c)
{
	if (c >= '0' && c <= '9')
		return c - '0';

	if (c >= 'A' && c <= 'F')
		return (c - 'A') + 10;

	return -1;
}


/*****************************************************************************
* ������� :   decode_ucs2
* ��������:   ����������� ������ ���� � ���������� � ������, �������� ��� ������.
			  UCS2 -> 1251 Windows Cyrillic. ������: [0x00,0x48,0x00,0x65,0x00,0x6C,0x00,0x6C,0x00,0x6F] - > "Hello"
* ���������:  byte_array - ������ ����, length - ��� �����
* ����������: ���������� ���������� ������ str
*****************************************************************************/
u8 *decode_ucs2(u8 *byte_array, u8 *length)
{
    u8 * str = (u8*)malloc(sizeof(u8) * (*length - is_bit_set(*length, 0)) + TERMINATE); // ���� ����� ��������, ���������� ��������� �����
	u8 i = 0;

	for (i = 0; i < (*length - is_bit_set(*length, 0)); i += 2)
	{
		if (*(byte_array + i) == 0x04)
			*(byte_array + i + 1) = *(byte_array + i + 1) - 0x10 + 0xC0;
		str[i / 2] = *(byte_array + i + 1);
	}
	str[i / 2] = '\0';

	return str;
}

/*****************************************************************************
* ������� :   hex_str_to_byte_array
* ��������:   ����������� ������ (������ ��������) "0B919700214365F7" -> [0x0B,0x91,0x97,0x00,0x21,0x43,0x65,0xF7]
* ���������:  str - UCS2 ������, len - � �����
* ����������: ��������������� ������ �������� buf_str
*****************************************************************************/
u8 *hex_str_to_byte_array(u8 *str, u8 *len)
{
    u8 *data = 0;
    int i = 0;
	*len = strlen(str) / 2;
	data = (u8*)malloc(sizeof(u8) * *len);

	for (i = 0; i < (2 * *len); i += 2)
		data[i / 2] = (u8)(char_to_u8(*(str + i)) << 4) | char_to_u8(*(str + i + 1));

	return data;
}

static size_t decode_stroctet(u8 *octet, size_t octet_sz, u8 *data, size_t data_sz)
{
    u8 msg_length = octet_sz;
    u8 *array_bytes = hex_str_to_byte_array(octet, &msg_length);
    u8 *decoded_str = decode_ucs2(array_bytes, &msg_length);
    strcpy(data, decoded_str);
	return msg_length < data_sz ? msg_length : data_sz;
}

static int decode7(u8 *in, size_t in_sz, u8 *out, size_t out_sz)
{
	int i = 0;
	int j = 0;
    u8 prev = 0;
	u8 new_str;
	int shift = 0;
    size_t out_len_max = (in_sz * 8) / 7;

    if ( out_len_max > out_sz ) {
        return -1;
    }

    for (i = 0, j = 0, shift = 8; (i < in_sz) && (j < in_sz); ++i, ++j, --shift ) {

        if ( shift == 1 ) {
            out[j] = (prev >> 1);
            ++j;
            shift = 8;
        }

        new_str = in[i];

        new_str <<= (8 - shift);
        new_str |= ((prev & (0xFF << shift)) >> shift);
        new_str &= ~0x80;
        out[j] = new_str;

        prev = in[i];
    }
    out[in_sz] = '\0';

    return 0;
}

/*****************************************************************************
* ������� :   sms_decode_pdu
* ��������:   ���������� ������ �� sms ������ pdu � ���������
*****************************************************************************/
int sms_decode_pdu(u8 *pdu_str, SMS_PDU_STRUCT *sms)
{
    unsigned int data_coding;

    u32 smc_length;
    u32 num_length;
    u32 sender_addr_type;

    u32 user_data_length;
    int decode_success = 0;

    if ( !pdu_str ) {
        return -1;
    }
    if ( !sms ) {
        return -1;
    }

    //������� sms ���������
    memset(sms, 0, sizeof(SMS_PDU_STRUCT));
    //bzero(sms, sizeof (sms_pdu));

    sscanf(pdu_str, "%02X", &smc_length);
    pdu_str += 2;
    //������� SMC ����������
    pdu_str += (smc_length * 2);
    //������� ������� ������ SMS-DELIVER ���������
    pdu_str += 2;

    //������ ����� ������ �����������
    sscanf(pdu_str, "%02X", &num_length);
    pdu_str += 2;
    sms->sender_length = num_length;
    num_length = ((num_length % 2) == 0 ? num_length : num_length + 1);
    //������ ���� ������ �����������
    sscanf(pdu_str, "%02X", &sender_addr_type);
    pdu_str += 2;
    sms->telnum_type = sender_addr_type;

    //������ ������ ��������
    decode_telnum(pdu_str, num_length, sms);
    pdu_str += num_length;
    //������� �������������� ���������
    pdu_str += 2;

    //���������� ����� ����������� ������
    sscanf(pdu_str, "%02X", &data_coding);
    pdu_str += 2;
    //������� ����� �������
    pdu_str += 14;
    //������ ����� ���������������� ������
    sscanf(pdu_str, "%02X", &user_data_length);
    pdu_str += 2;

    if ( data_coding == 0x00 ) {
        size_t decode_sz = 0;
        u8 msg_length = user_data_length * 2;
        u8 *decoded_data = hex_str_to_byte_array(pdu_str, &msg_length);

        msg_length < MAX_SMS_MESSAGE_SIZE ? (decode_sz = msg_length) : (decode_sz = MAX_SMS_MESSAGE_SIZE);
        if (decode_sz >= 0) {
            //������������� 8 ��� � 7 ���
            int dec7_res = decode7(decoded_data, user_data_length, sms->msg, MAX_SMS_MESSAGE_SIZE);
            if (dec7_res == 0 ) {
                //��������� ����� ������
                sms->msg_length = user_data_length;
                decode_success = 1;
            }
        }
    }
    else if ( data_coding == 0x04 ) {
        //������������� �������� ������
        size_t decode_sz = decode_stroctet(pdu_str, user_data_length * 2, sms->msg, MAX_SMS_MESSAGE_SIZE);
        if ( decode_sz >= 0 ) {
            sms->msg_length = decode_sz;
            decode_success = 1;
        }
    }
    else if ( data_coding == 0x08 ) {
        size_t decode_sz;
        decode_sz = decode_stroctet(pdu_str, user_data_length * 4, sms->msg, MAX_SMS_MESSAGE_SIZE);
        //decode_ucs2(pdata, user_data_length * 4);
        if ( decode_sz >= 0 ) {
            sms->msg_length = decode_sz;
            decode_success = 1;
        }
    }
    return (decode_success ? 0 : -1);
}

/*****************************************************************************
* ������� :   encode_phone_num
* ��������:   �������� ������ "+79001234567" � PDU ������ � ���� ������ "0B919700214365F7" � ������������ � ��� ������ �������: [0x0B, 0x91,0x97,0x00,0x21,0x43,0x65,0xF7]
* ���������:  phone_numb - ����� ���������� � �������� ������������ �������, len - � �����
* ����������: ������ ����
*****************************************************************************/
static u8 *encode_phone_num(u8 *phone_numb, u8 *len)
{
    u8 reserv_buf[MAX_NUMBER_LENGTH + TERMINATE] = { 0 };
    u8 out_buf[MAX_NUMBER_LENGTH + TERMINATE] = { 0 };
    u8 *address = 0;
	int header = 0;
    memcpy(reserv_buf, phone_numb, strlen(phone_numb));
    address = reserv_buf;

    if (*address == '+')
    {
        //������� ������ "+" �� ������ �������� ����������
        memcpy(address, address + 1, strlen(address));
        header = (strlen(address) << 8) + 0x81 | 0x10; //International number
	}
	else
        header = (strlen(address) << 8) + 0x81 | 0x20; //National number

	//���� ����� �� ������, �� ��������� � ����� ������ "F"
    if (strlen(address) % 2 == 1)
        *(address + strlen(address)) = 'F';

    sprintf(out_buf, "%04X", header);
    reverse(address);
    //sprintf(out_buf, "%s%s", out_buf, address);
    strcat(out_buf, address);
    return hex_str_to_byte_array(out_buf, len);
}

/*****************************************************************************
* ������� :   msg_to_ucs2
* ��������:   �������� ������ ���������, �������� "Hello" � PDU ������ � ���� ������ UCS2 ��������� "00480065006C006C006F" � ������������ � ��� ������ �������: [0x00,0x48,0x00,0x65,0x00,0x6C,0x00,0x6C,0x00,0x6F]
* ���������:  src_msg - ��������� �� �������������� � ��������, len - ��� �����
* ����������: ������ ����
*****************************************************************************/
static u8 *msg_to_ucs2(char *src_msg, u8 *len)
{
    ascii *src_s;
    u16 ushort_code_char = 0;
    u8 dest_msg[MAX_UCS2_MSG_LEN * 4 + TERMINATE_SYMBOL] = { 0 };

    for (src_s = src_msg; *src_s != '\0'; ++src_s)
	{
        char hex[TWO_OCTET_LENGTH + 1] = { '0','0','0','0','\0' };
        //� = -62; � = -32; � = -33; � = -1
        //� = 192; � = 224; � = 223; � = 255
        //code = *src_s - (UCHAR_MAX + 1);
        //if( (code <= -1 && code >= -32) || (code >= -64 && code <= -33))

        if(*src_s > 191 && *src_s < 256)
            ushort_code_char = USHRT_MAX + *src_s - UCHAR_MAX; //'�' => 65535 + 223 - 255 = 65503
        else
            ushort_code_char = USHRT_MAX + *src_s + 1;

        convert_char_to_ucs2(ushort_code_char, hex); //���� ������ ������ ������ � ���������� ����������������� ����� � hex
        strcat(dest_msg, hex);
    }
    return hex_str_to_byte_array(dest_msg, len);
}

/*****************************************************************************
* ������� :   sms_encode_pdu
* ��������:   �������� ��������� � ������ PDU ������� � ��������� UCS2, ��� ����� ��������. �������� ����� 70 �������� sms
* ���������:  address - ����� ���������� � �������� ������������ �������, message - ��������� �� ��������
* ����������: PDU ��������� �� ��������
*****************************************************************************/
u8 **sms_encode_pdu( u8 *address, u8 *message) //������ ��������� UCS2, ��� VP
{
    u16 j;
    u16 i = 0;
    u8 **pdu_strs = {0};
    u16 total_msg_len = 0;
    u16 msgs_count = 0;
    u16 msg_counter = 1;
    u8 msg_ref = 0;
    u8 encoded_adress_len = 0;
    u8 encoded_msg_len = 0;
    u8 ref_num = 0;
    u8 pdu[PDU_LENGTH] = { 0 };
    u8 msg[MAX_UCS2_MSG_LEN + TERMINATE] = { 0 };
    u8 *ptr = NULL;
    u8 *encoded_adress = NULL;
    u8 *encoded_msg = NULL;

    total_msg_len = strlen(message);
    //DEBUG_TRACE("����� ���������: %d", total_msg_len);
    msgs_count = (total_msg_len / MAX_UCS2_MSG_LEN) + ((total_msg_len % MAX_UCS2_MSG_LEN > 0) ? 1 : 0);
    //DEBUG_TRACE("���������� ��������� �� ��������: %d", msgs_count);

    //��������� ������ ��� ��������� ������
    pdu_strs = (u8 **) malloc((msgs_count + 1) * sizeof(u8));
    pdu_strs[0] = (u8 *) malloc(2 * sizeof(u8));
    for (j = 1; j < msgs_count + 1; j++)
        pdu_strs[j] = (u8 *) malloc(PDU_LENGTH * sizeof(u8)); //������ ����� �������� � pdu_strs[i][j]

    sprintf(pdu_strs[0],"%d",msgs_count);

    encoded_adress = encode_phone_num(address, &encoded_adress_len);

    ref_num = 0 + rand() % 255; //��������� reference number, 1 ����� concat ���������

    for (i = 0; i < total_msg_len; i += MAX_UCS2_MSG_LEN)
    {
        memset(pdu, 0, sizeof pdu); //������������� ����� � ������ ��� ������
        memset(msg, 0, sizeof msg);

        memcpy(msg, message + i, MAX_UCS2_MSG_LEN);

        ptr = pdu; //��������� ptr � pdu

        *ptr++ = 0x00; //(����� ���������� ������) ����� ���������� SMSC. ����� ����� ����� 0, ��� �������� -
        //- ���������� ������������ SMSC, ���������� � ��������
        *ptr++ = (msgs_count > 1) ? 0x41 : PDU_TYPE; //������� SMS ���������, ��������� �� �������� ������������
        *ptr++ = msg_ref++;  //������ �� ���������. ������ ����������� �� 1

        memcpy(ptr, encoded_adress, encoded_adress_len); //����� ����������
        ptr += encoded_adress_len;

        *ptr++ = 0x00; //TP-PID (������������� ���������)
        *ptr++ = 0x08; //TP-DCS � ����� ����������� TP-������ 0x08 - UCS2

        encoded_msg = msg_to_ucs2(msg, &encoded_msg_len);

        if (msgs_count > 1) //���� UDH (UD Header Concat Msg)
        {
            *ptr++ = encoded_msg_len + 0x06;
            *ptr++ = 0x05;
            *ptr++ = 0x00;
            *ptr++ = 0x03;
            *ptr++ = ref_num;
            *ptr++ = msgs_count;
            *ptr++ = msg_counter++;
        }
        else
            *ptr++ = encoded_msg_len +  0x00;

        memcpy(ptr, encoded_msg, encoded_msg_len);
        ptr = ptr + encoded_msg_len;

        //���������� ��������� PDU ������ � ��������� ������, �������� PDU ��������� �� ��������
        strcpy(pdu_strs[msg_ref],bytes_array_to_hex_string(pdu, ptr - pdu));

        free(encoded_msg);
    }

    free(encoded_adress);

    /*
    j = 0;
    while(strlen(pdu_strs[j]) > 0)
    {
        DEBUG_TRACE("%d MSG: %s", j, pdu_strs[j]);
        j++;
    }
    */

    if(strlen(pdu_strs[0]) > 0)
        return pdu_strs;
}
