#ifndef SMS_PDUNICODE_H
#define	SMS_PDUNICODE_H

#include <string.h>
#include "custom_types.h"

#pragma once

/* ������� */
#define BYTE 0x08
#define PDU_TYPE 0x01 //�� ��������� ��� �������� PDU ��������� - 7 ���
#define TERMINATE 0x01
#define TERMINATE_SYMBOL 1
#define TWO_OCTET_LENGTH 4

#define SMS_SENDER_SIZE 25 //����. ����� ������ �����������
#define MAX_NUMBER_LENGTH 16
#define MAX_SMS_MESSAGE_SIZE 161 //������������ ����� SMS ���������
#define MAX_UCS2_MSG_LEN 67  //������������ ����� SMS ��������� � ��������� UCS2
#define PDU_LENGTH 1024

typedef struct {
    u8 msg[MAX_SMS_MESSAGE_SIZE]; //���������
    u16 msg_length; //����� ���������
    char telnum[SMS_SENDER_SIZE]; //����� ��������
    size_t sender_length; //����� �����������
    u8 telnum_type; //��� ������ ��������
} SMS_PDU_STRUCT;

/*****************************************************************************
* �������  : sms_decode_pdu
* �������� : �������������� ������ �� PDU ������ SMS ���������, ����������� �������.
* ���������:
*     pdu_str - ������ SMS ��������� � ������� PDU, ���������� ������� GSM ������.
*     sms - ��������� PDU SMS ��������� SMS_PDU_STRUCT.
* ����������: int - ���� ��� �������� ����������.
*****************************************************************************/
int sms_decode_pdu(u8 *pdu_str, SMS_PDU_STRUCT *sms);

/*****************************************************************************
* �������  : sms_encode_pdu
* �������� : ��� ������� �������� ��������� PDU �� ������� ������.
 * ����������, �� �������� ��������� ��������� ��� ���� � ��������� sms_t, ���������� ���� �������.
 * ��� �������� ���������� ������� ���������� ����� �������������� ������ PDU.
* ���������:
*     address - ����� ���������� ���������.
*     message - ��������� �� ��������.
* ����������: ������������ ��������� � ���� PDU ������.
*****************************************************************************/
u8 **sms_encode_pdu(u8 *address, u8 *message);

#endif	/* SMS_PDUNICODE_H */
