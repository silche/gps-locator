/*---------------------------------------------------------------------
* ������������ ����� :   geosdata.c
* ��������           :   ���������� � �������� ��������� � ���� ���������
* ���� ������        :   21.01.2017
*---------------------------------------------------------------------*/

/* ������������ ����� */
#include <stdio.h>
#include "math.h"
#include "geodata.h" // ��������� �� �������� ��������

/* ��������� ���������� (STATIC) */
static LOCATION last_gps_info;
static LOCATION *last_gps = &last_gps_info;

/* ��������� ������� */

static eat_bool manager_ATcmd_flag = FALSE;
/*
*get the last gps info
*/
LOCATION *get_last_gps(void)
{
    return last_gps;
}

//���������� ��������� GPS ���������� (���������� ������ � ��������������� ���������)
int gps_save_last(LOCATION *location_data)
{
    last_gps_info.isGps = location_data->isGps;

    if(last_gps_info.isGps == TRUE)
    {
        DEBUG_TRACE("Save GPS data."); //GPS �����������, ���������� ������ �� ���������
        last_gps_info.gpsData = location_data->gpsData;
    }
    else
    {
        DEBUG_TRACE("Save first Cell data."); //C��������� ���������� � ������� ������� ������� ����
        last_gps_info.cellInfo = location_data->cellInfo;
    }
    return TRUE;
}
