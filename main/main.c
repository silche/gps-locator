/*---------------------------------------------------------------------
* ������������ ����� :   main.c
* ��������           :   ������� ���� ���������, ���������� ����� ����� � �������� ���� ��������� - ����� "app_main"
* ���� ������        :   24.02.2017
*---------------------------------------------------------------------*/

/******************************************************************************
*  ������������/������������ �����
******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <eat_modem.h>
#include <eat_socket.h>
#include <eat_clib_define.h> //only in main.c

#include "custom_types.h"
#include "event.h"
#include "app_gps.h" // ��������� �� �������� ��������

#include "settings.h"
#include "setup_config.h"
#include "tcpip.h"

/********************************************************************
* �������
********************************************************************/
#define EAT_DEBUG_STRING_INFO_MODE //������ � ���������� ����������� ��� ������ �� ���� �������

#define EAT_MEM_MAX_SIZE 100*1024
/********************************************************************
* ����������� �����
********************************************************************/
typedef void(*app_user_func)(void*);

/********************************************************************
* ��������� �����������:  STATIC
********************************************************************/

static u8 s_memPool[EAT_MEM_MAX_SIZE];

/********************************************************************
* ���������� ������� �������
********************************************************************/
extern void APP_InitRegions(void);

/********************************************************************
* ���������� ��������� �������
********************************************************************/
void app_main(void *data);
void app_func_ext1(void *data);
void app_entry(void);

#pragma arm section rodata = "APP_CFG"
APP_ENTRY_FLAG
#pragma arm section rodata

#pragma arm section rodata="APPENTRY"
const EatEntry_st AppEntry =
{
    app_main, //�������� ���������
    app_func_ext1,
    (app_user_func)app_gps_thread,//app_user1, ����������� ����� ��� ������ � ��������� ������ � GPS ������
    (app_user_func)EAT_NULL,//app_user2,
    (app_user_func)EAT_NULL,//app_user3,
    (app_user_func)EAT_NULL,//app_user4,
    (app_user_func)EAT_NULL,//app_user5,
    (app_user_func)EAT_NULL,//app_user6,
    (app_user_func)EAT_NULL,//app_user7,
    (app_user_func)EAT_NULL,//app_user8,
    EAT_NULL,
    EAT_NULL,
    EAT_NULL,
    EAT_NULL,
    EAT_NULL,
    EAT_NULL
};
#pragma arm section rodata

/********************************************************************
* ��������� �������
********************************************************************/
static void app_func_ext1(void *data)
{
    /*
��� ������� ����� ���� ������� ����� ������� ���������� �������, ���������� ���������� GPIO, UART � �. �.
������ ��� API ����� ������������:
eat_uart_set_debug: ��������� ����� �������
eat_pin_set_mode: ��������� ������ GPIO
eat_uart_set_at_port: ��������������� AT ����
*/
#ifdef EAT_DEBUG_STRING_INFO_MODE
    EatUartConfig_st cfg =
    {
        EAT_UART_BAUD_115200, //�������� - ����
        EAT_UART_DATA_BITS_8, //������ ������ (1 ����)
        EAT_UART_STOP_BITS_1, //�������� ���
        EAT_UART_PARITY_NONE //��� ��������
    };
#endif
    eat_uart_set_at_port(eat_uart_at);
    eat_uart_set_debug(eat_uart_debug); //������������� USB � �������� ����� ��� �������
    //��������� ������ � ���������� ����������� ��� ������ �� ���� �������
#ifdef EAT_DEBUG_STRING_INFO_MODE
    //�����������
    if (EAT_UART_USB == eat_uart_debug)
        eat_uart_set_debug_config(EAT_UART_DEBUG_MODE_UART, NULL);
    else
        eat_uart_set_debug_config(EAT_UART_DEBUG_MODE_UART, &cfg);
#endif
}

//��������� ������������� �������������� GSM-������
static u8 *get_Battery_props(void)
{
    EAT_CBC_ST bmt={0};
    u8 bat_stat[50] = {0};
    eat_get_cbc(&bmt);
    DEBUG_TRACE("Battery: status=%d, level=%d, voltage=%d\n", bmt.status, bmt.level, bmt.volt);
    sprintf(bat_stat, "Battery level: %d, mVolt: %d", bmt.level, bmt.volt);
    return bat_stat;
}

void app_entry(void)
{
    EatEvent_st event; //���������� �������
    u32 event_num = 0;

    //������������� ����������������� ����� (����) � GPS ������
    if(uart_initialize() == TRUE)//UART
        DEBUG_TRACE("���� ��������!");

    if(gps_module_init() == TRUE)//GPS
        DEBUG_TRACE("������ GPS ��������!\n");

    //���� �� ������� � ������� ���� � �����������
    if(!Restore_Settings())
    {
        DEBUG_TRACE("� ������� ��� ����������� ��������");
        Save_Settings();
    }
    else if(setts.phone_Number == NULL) //�� ����� ����� ������������ (���������)
    {
        DEBUG_TRACE("� ������� �� �������� ����� ������������");
        //������� ����������� � ������� 10 �����
        setts.main_app_timer_period = 15 * 60 * 1000; //��������� ������� ������ �������� ���������
        //���� � ������� ����� ������� �� ����� ����� ����� ���������, ���������� ����������
    }
    simcom_sms_init(); //������������� ������ � ����������� SMS

    DEBUG_TRACE("\nAPP ENTRY!\n");

    //����������� ������������� �������
    get_Battery_props();

    while (TRUE) //�������� ���� ��������� ��� ���������� ������
    {
        event_num = eat_get_event_num(); //��������� ���������� �������
get_event:
        if(event_num > 0)
        {
            eat_get_event(&event); //�������� �������
            //DEBUG_TRACE("C������ [%s] event_id=%d", __FUNCTION__, event.event); //����� �������������� �������

            if (event.event == EAT_EVENT_NULL || event.event >= EAT_EVENT_NUM)
            {
                DEBUG_TRACE("[%s] ����������� ��� �������", __FUNCTION__, event.event);
                continue;
            }

            event_handler(&event); //����� ����������� �������

            event_num = eat_get_event_num(); //��������� ���������� �������
            if(event_num > 0)
                goto get_event;
        }
    }
}

//����� ����� � ���������
static void app_main(void *data)
{
    EatEntryPara_st *para;
    eat_bool rt;

    APP_InitRegions(); //������������� ����������� ������ (RAM) ��� ����������� ��������� app
    APP_init_clib();  //������������� ��������� ����� C

    para = (EatEntryPara_st*)data;

    //memcpy(&app_para, data, sizeof(EatEntryPara_st));
    //if(app_para.is_update_app && app_para.update_app_result)
    if(para->is_update_app && para->update_app_result)
    {
        //APP ������� ��������� (APP update succeed)
        DEBUG_TRACE("APP upgrade success");
        eat_update_app_ok(); //�������� ����� ���������� ��������� APP (clear update APP flag)
    }

    //DEBUG_TRACE("booting: version:%s, build_time=%s %s. core(version:%s, buildno=%s, buildtime=%s)",
    //        VERSION_STR, __DATE__, __TIME__, eat_get_version(), eat_get_buildno(), eat_get_buildtime());

    //������������� ������
    //app_mem_init();
    rt = eat_mem_init(s_memPool, EAT_MEM_MAX_SIZE);
    if (!rt)
    {
        DEBUG_TRACE("eat memory initial error:%d!", rt);
        return;
    }
    app_entry(); //���� � �������� ���� ����������
}
