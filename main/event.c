/*---------------------------------------------------------------------
* ������������ ����� :   event.�
* ��������           :   ��������� �������
* ���� ������        :   25.02.2017
*---------------------------------------------------------------------*/

/* ������������ ����� */
#include <string.h>
#include "custom_types.h"
#include "timer.h"
#include "gps_tools.h"
#include "tcpip.h"
#include "settings.h"
#include "modem.h"
#include "mem.h"
#include "uart.h"
#include "app_sms.h"

/* ���� */
typedef int (*EVENT_FUNC)(const EatEvent_st *event);

/* ������� */
#define MAX_DEBUG_BUF_LEN 256   //128 for loginfo is not enough
#define DESC_DEF(x) case x:\
    return #x

/* ��������� ������� */
static char *getEventDescription(EatEvent_enum event)
{
    switch (event)
    {
#ifdef APP_DEBUG
    DESC_DEF(EAT_EVENT_TIMER);
    DESC_DEF(EAT_EVENT_KEY);
    DESC_DEF(EAT_EVENT_INT);
    DESC_DEF(EAT_EVENT_MDM_READY_RD);
    DESC_DEF(EAT_EVENT_MDM_READY_WR);
    DESC_DEF(EAT_EVENT_MDM_RI);
    DESC_DEF(EAT_EVENT_UART_READY_RD);
    DESC_DEF(EAT_EVENT_UART_READY_WR);
    DESC_DEF(EAT_EVENT_ADC);
    DESC_DEF(EAT_EVENT_UART_SEND_COMPLETE);
    DESC_DEF(EAT_EVENT_USER_MSG);
    DESC_DEF(EAT_EVENT_IME_KEY);
#endif
    default:
    {
        static char soc_event[10] = {0};
        snprintf(soc_event, 10, "%d", event);
        return soc_event;
    }
    }
}

bool user_registered = FALSE;

//������ ������ ���� �������� ��������
static void start_mainloop(void)
{
    DEBUG_TRACE("������ ����������� ��������.\n");
    eat_timer_start(TIMER_MAIN_LOOP, setts.main_app_timer_period);
    //������ �������, ���������������� ��� ��������� GPS ������, ����������� �� ����, �
    //�������� �� �� ������ ����� ����������� �������� �������
    eat_timer_start(TIMER_GPS_SEND, setts.gps_send_period);
}

//�������� GPS ������ �� ������ �����������
static int cmd_sendGPS2Server(void)
{
    u8 *gps_wialonbuf; //�����, �������� ������ � ������������ � ������� Wialon IPS
    size_t w_buf_len = 0;

    //�������� GPS ������ �� ������ ������������
    gps_wialonbuf = assembly2Wialon();

    w_buf_len = strlen(gps_wialonbuf);
    if(w_buf_len > 0) //�������� GPS ������ �� ������ ������������,  ���� ������ ���������� � �����
    {
        DEBUG_TRACE("�������� GPS ������ �� ������ �����������...\n");
        socket_send_data(gps_wialonbuf, w_buf_len); //�������� ��������������� ������ �� ������ ������������
    }
    return 0;
}

//����� ���������� �� ��������� ������-���� �������
static int timer_proc(const EatEvent_st *event)
{
    switch (event->data.timer.timer_id)
    {
    case TIMER_MAIN_LOOP:
        DEBUG_TRACE("TIMER_MAIN_LOOP ����������.");

        if(user_registered)
        {
            eat_sleep(setts.sleep_timer_period); //���� ��������� � ����� ���

            //����� �����������
            sms_send("", "����� �� �����!"); //�������� ��������� �� ����� ��������� �����
            eat_timer_start(event->data.timer.timer_id, setts.main_app_timer_period);
        }
        else
            eat_power_down();
        break;

    case TIMER_GPS_SEND:
        if(user_registered)
            cmd_sendGPS2Server();//�������� GPS-������ �� ������ �����������
        eat_timer_start(event->data.timer.timer_id, setts.gps_send_period);
        DEBUG_TRACE("TIMER_GPS_SEND: %d �������\n", event->data.timer.timer_id);
        break;
    default:
        DEBUG_TRACE ("� ������� %d ��� �����������!", event->data.timer.timer_id);
        break;
    }
    return 0;
}

static int event_adc(const EatEvent_st *event)
{
    unsigned int value = event->data.adc.v;
    DEBUG_TRACE("ad value=%d", value);
    /*if (event->data.adc.pin == ADC_433)
        seek_proc(value);
    else
        DEBUG_TRACE("not processed adc pin:%d", event->data.adc.pin); */
    return 0;
}

static int event_mdm_rx_proc(const EatEvent_st *event)
{
    u8 rx_buf[256] = {0};
    u16 len = 0;
    char *ussd_resp = 0;

    len = eat_modem_read(rx_buf, 256);
    if (!len)
    {
        DEBUG_TRACE("������. Modem received nothing: len = 0");
        return -1;
    }
    rx_buf[len] = '\0';
    DEBUG_TRACE("[%s] Modem recieve: %s", __FUNCTION__, rx_buf); //��������

    ussd_resp = modem_IsUSSDrequ(rx_buf); //���������� �� � ������ ����� �� USSD ������
    if(ussd_resp != 0)
    {
        u8 str_to_send[161] = {0}; //������ �� ��������
        u16 i = 0;
        //�������� ������ ����� � ������ � �����, �������� ����� �� �������� �� ������� - " ��� �� ������� ����� ������
        if(strstr(ussd_resp,"\n"))
            while(ussd_resp[i] != '\n')
                i++;
        if(i > strlen(ussd_resp)-2 && strstr(ussd_resp,"\""))
        {
            i = strlen(ussd_resp);
            while(ussd_resp[i] != '\"')
                i--;
        }
        strncpy(str_to_send, ussd_resp, i+1);//���������� � ����� ��� �������� �� SMS
        str_to_send[i] = '\0'; //������ ������ ����� ������
        DEBUG_TRACE("������ �� ��������: %s", str_to_send);
        modem_cancelUSSD_session(); //������� �� USSD ����
        sms_send("", str_to_send);
    }
    else if (modem_IsCallReady(rx_buf)) //���������� � ���������� ������ ��� ������� "Call Ready"
    {
        DEBUG_TRACE("GSM ������ �����\n");
    }
    else if (modem_IsSMSReady(rx_buf))
    {
        if(strcmp(setts.phone_Number, "") && strlen(setts.phone_Number) != 0) //���� ������������ ��������������� � �������
        {
            user_registered = TRUE;
            sms_send("","����� � ������!"); //�������� ��������� �� ����� ��������� �����

            start_mainloop(); //������ �������� ��������
            if (modem_GPRSAttach())
                DEBUG_TRACE("GPRS attach success.");
            else
            {
                DEBUG_TRACE("����������� � ���� �������� �� GPRS");
                //simcom_tcpip_gprs_start();

                if (modem_GPRSAttach())
                    DEBUG_TRACE("GPRS attach success.");
            }
        }
        //modem_AT("AT+CSCS=\"GSM\"");
    }
    else if(modem_IsCCIDOK(rx_buf))
    {
        DEBUG_TRACE("modem CCID OK: %s", modem_IsCCIDOK(rx_buf));
    }
    return 0;
}

//�������� ���������� �������
int event_handler(EatEvent_st *event)
{
    EVENT_FUNC proc_func = EAT_NULL;

    DEBUG_TRACE("�������: %s", getEventDescription(event->event));

    switch (event->event) //�������
    {
    case EAT_EVENT_TIMER:
        proc_func = timer_proc;
        break;
    case EAT_EVENT_MDM_READY_RD:
        proc_func = event_mdm_rx_proc;
        break;
    case EAT_EVENT_MDM_READY_WR:
        DEBUG_TRACE("[%s] modem event :%s", __FUNCTION__, "EAT_EVENT_MDM_READY_WR");
        break;
    case EAT_EVENT_UART_READY_RD: //������ ������� ����������, ����� �������� ������ � GPS ������
        proc_func = event_uart_rx_proc;
        break;
    case EAT_EVENT_UART_READY_WR:
        DEBUG_TRACE("[%s] uart(%d) event :%s", __FUNCTION__, event->data.uart, "EAT_EVENT_UART_READY_WR");
        proc_func = event_uart_wr_proc;
        break;
    case EAT_EVENT_UART_SEND_COMPLETE:
        proc_func = uart_send_complete_proc;
        break;
    case EAT_EVENT_USER_MSG:
        //proc_func = EAT_NULL;
        //proc_func = event_threadMsg;
        break;
    case EAT_EVENT_ADC:
        proc_func = event_adc;
        break;
    default:
        break;
    }

    if (proc_func)
    {
        //����� ������ ��������� ���������������� �������
        proc_func(event);
        return 0;
    }
    else
    {
        DEBUG_TRACE("event(%s) not processed!", getEventDescription(event->event));
        return -1;
    }

    DEBUG_TRACE("event(%s) has no handler!", getEventDescription(event->event));
    return -1;
}
