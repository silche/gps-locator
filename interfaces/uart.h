#ifndef UART_H_
#define UART_H_

#include "custom_types.h"
#include <eat_uart.h>

#define EAT_AT_TEST
#define EAT_UART_RX_BUF_LEN_MAX 2000 //������������ ����� ������

u8 *get_uart_Buf(void);

int event_uart_rx_proc(const EatEvent_st *event);
int event_uart_wr_proc(const EatEvent_st *event);
void uart_send_complete_proc(const EatEvent_st *event);

typedef int UART_WRITER(void);
void uart_setWrite(UART_WRITER writer);

#endif /* UART_H_ */
