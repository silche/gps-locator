/*---------------------------------------------------------------------
* ������������ ����� :   spi.c
* ��������           :   ������������� � �������� ���������� �� ����������������� ������������� ���������� SPI
*                        ��������� ������ �� �������������
*---------------------------------------------------------------------*/

/* ������������ ����� */
#include "spi.h"

/* ��������� ������� */
//������� ����� ������� � ������, ��������� �� ���� �����

static EatGpioLevel_enum get_level(u8 bit)
{
    if(bit == 0)
        return EAT_GPIO_LEVEL_LOW;
    else if(bit==1)
        return EAT_GPIO_LEVEL_HIGH;
}

static void clock_SPC(void)
{
    eat_gpio_write(SCLK, EAT_GPIO_LEVEL_LOW);
    eat_gpio_write(SCLK, EAT_GPIO_LEVEL_HIGH);
}

//��������� ������ ����� SPI ��������� of len bytes
u16 *spi_read_data(void)
{
    u8 clk = 0;
    u16 *SPIData = 0;
    for(clk = 0; clk < 8; clk++)
    {
        //eat_gpio_write(MOSI, EAT_GPIO_LEVEL_LOW);
        SPIData[clk] = eat_gpio_read(MISO);
        clock_SPC();
    }
    return SPIData;
}
//�������� ������� �� SPI
u16 *spi_write_command(u16 *bits)
{
    u8 clk = 0;
    //������ ���, ����������� ������ ������ �� ������������� ��� ���������� � ����
    //���� = 0, �� ����� � ����������, ���� 1, �� ������ ���
    eat_gpio_write(MOSI, EAT_GPIO_LEVEL_HIGH);
    for(clk = 0; clk < 8; clk++)
    {
        eat_gpio_write(MOSI, get_level(bits[clk]));
        clock_SPC();
    }
    return spi_read_data();
}

//������������� ������������ SPI
void spi_initialize(void)
{	
	DEBUG_TRACE("GPIO INIT");
	eat_pin_set_mode(MOSI, EAT_PIN_MODE_GPIO);
    eat_pin_set_mode(MISO, EAT_PIN_MODE_GPIO);
	eat_pin_set_mode(SCLK, EAT_PIN_MODE_GPIO);
    eat_pin_set_mode(CS, EAT_PIN_MODE_GPIO);

	//��������� cheap select - CS
	//���� CS = 1, �� ������ �� ����������
	//���� 0, �� ���������� ������� � ����� ���������� ������
    eat_gpio_setup(CSSTM, EAT_GPIO_DIR_OUTPUT, EAT_GPIO_LEVEL_HIGH);
	eat_gpio_setup(CS, EAT_GPIO_DIR_OUTPUT, EAT_GPIO_LEVEL_HIGH);
	eat_gpio_setup(SCLK, EAT_GPIO_DIR_OUTPUT, EAT_GPIO_LEVEL_HIGH);
	
    eat_gpio_setup(MISO, EAT_GPIO_DIR_INPUT, EAT_GPIO_LEVEL_LOW);
	//��������� ������� ������������, ����� ������ ���������� �������� EAT_GPIO_DIR_OUTPUT
    eat_gpio_setup(MOSI, EAT_GPIO_DIR_OUTPUT, EAT_GPIO_LEVEL_LOW);
}

void spi_test(void)
{
    u16 bindata[8] = {0,0,0,1,1,1,1}; //8F - Who am I?
    u16 *SPIData = 0; //33 (00110011) - then OK
	DEBUG_TRACE("Write SPI CMD");

	//���������� �� ������/������
    CS_ON();
    SPIData = spi_write_command(bindata);
	CS_OFF();
    DEBUG_TRACE("SPIData: %d%d%d%d%d%d%d%d", SPIData[0],SPIData[1],SPIData[2],SPIData[3],SPIData[4],SPIData[5],SPIData[6],SPIData[7]);
}
