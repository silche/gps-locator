#ifndef SPI_H
#define SPI_H

#include <eat_periphery.h>
#include "custom_types.h"

//������ ����������
#define MISO EAT_PIN3_UART1_RTS
#define MOSI EAT_PIN4_UART1_CTS
#define SCLK EAT_PIN5_UART1_DCD
#define CSSTM EAT_PIN6_UART1_DTR
#define CS   EAT_PIN7_UART1_RI

#define CS_ON()		eat_gpio_write(CS, EAT_GPIO_LEVEL_LOW)
#define CS_OFF()	eat_gpio_write(CS, EAT_GPIO_LEVEL_HIGH)

/*
output X: SIM800C			  - accelerometer
       3: EAT_PIN3_UART1_RTS(SPI_MISO) - SDA/MI //�������� ������ �� �������������
       4: EAT_PIN4_UART1_CTS(SPI_MOSI) - SA0/MO //����� ������ � ������������
       5: EAT_PIN5_UART1_DCD(SPI_SCK)  - SCL/CLK
       6: EAT_PIN6_UART1_DTR(SPI_CS1)  - CS (STM8)
       7: EAT_PIN7_UART1_RI(SPI_CS2)   - CS (Accelerometer)
*/

//�������� ������ ��� ������ � STM8
#define SET_TIME	0x01
#define GET_TIME	0x02
#define SET_ALARM	0x03
#define GET_ALARM	0x04
#define GET_STATUS	0x05
#define GET_BATTERY	0x06
#define SLEEP	0x07

void spi_test(void);
void spi_initialize(void);
u16 *spi_write_command(u16 *bits);
void spi_write_data(u16 *bits);
u16 *spi_read_data(void);

void Set_Time (u8 years, u8 mon, u8 day, u8 hours, u8 mins, u8 secs);
void Get_Time(void);
void Set_WakeUp_Time (u8 hours, u8 minutes, u8 seconds);
void Get_WakeUp_Time (void);
void Go_to_Sleep (void);

#endif /* SPI_H_ */
