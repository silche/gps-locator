#ifndef SOCKET_TCPIP_H_
#define SOCKET_TCPIP_H_

#include <eat_socket.h>
#include "custom_types.h"

#define DBG(...) DEBUG_TRACE(__VA_ARGS__)
#define DBG_E(...) DEBUG_TRACE(__VA_ARGS__)

#define APN_NAME_LEN						20
#define APN_USER_NAME_LEN					20
#define APN_PASSWORD_LEN					20
#define SERVER_IP_LEN				        20
#define FTP_SERVER_USER_NAME_LEN			20
#define FTP_PASSWORD_LEN					20
#define FTP_FILENAME_LEN					20
#define FTP_FILEPATH_LEN					20
#define FTP_PORT_LEN						5

typedef struct ModemConfigContextTag
{
    ascii apnName[APN_NAME_LEN+1];
    ascii apnUserName[APN_USER_NAME_LEN+1];
    ascii apnPassword[APN_PASSWORD_LEN+1];
    ascii FTPServerIP[SERVER_IP_LEN+1];
    ascii ftpUserName[FTP_SERVER_USER_NAME_LEN+1];
    ascii ftpPassword[FTP_PASSWORD_LEN+1];
    ascii ftpFileName[FTP_FILENAME_LEN+1];
    ascii ftpFilePath[FTP_FILEPATH_LEN+1];
    u16 ftpPort;
}ModemConfigContext;

extern ModemConfigContext g_modemConfig;

//extern s8 socket_id;
typedef void (*ResultNotifyCb)(eat_bool result);

/* ���������� ������� */
eat_bool simcom_fota_update(u8 *username, u8 *pswd, u8 *getName, u8 *getPath, u8 *serv, u8 port);
s8 simcom_connect_server(u8 ip_addr[4]);
s32 simcom_send_data_to_server(s8 s_id, const void *databuf, s32 len);
s32 simcom_recv_from_server(s8 s_id, void *databuf, s32 len);
eat_bool simcom_gethostbyname(const char *domain_name);
s32 socket_gprs_setup(void);
s32 socket_tcpip_init(void);
s32 socket_send_data(const void *databuf, s32 len);

void simcom_tcpip_gprs_start(void);
#endif
