/*---------------------------------------------------------------------
* ������������ ����� :   socket_tcpip.c
* ��������           :   �������������� � ��������-�������� �� ���������� GPRS
* ���� ������        :   24.02.2017
*---------------------------------------------------------------------*/
#include "tcpip.h"
#include "settings.h"
#include "errors.h"

static s8 socket_id = -1;
static u32 request_id = 0;

#define DESC_DEF(x)	case x:\
    return #x
/*
 * ����������� ���
SOC_READ    = 0x01,   ������
SOC_WRITE   = 0x02,   ������
SOC_ACCEPT  = 0x04,   ��������/�������������
SOC_CONNECT = 0x08,   �����������
SOC_CLOSE   = 0x10,   ��������
SOC_ACKED   = 0x20,   acked
*/
static char *get_EventDescription(soc_event_enum event)
{
    switch (event)
    {
#ifdef APP_DEBUG
    DESC_DEF(SOC_READ);
    DESC_DEF(SOC_WRITE);
    DESC_DEF(SOC_ACCEPT);
    DESC_DEF(SOC_CONNECT);
    DESC_DEF(SOC_CLOSE);
    DESC_DEF(SOC_ACKED);
#endif
    default:
    {
        static char soc_event[10] = {0};
        snprintf(soc_event, 10, "%d", event);
        return soc_event;
    }
    }
}

/*
CBM_DEACTIVATED             = 0x01,  ���������
CBM_ACTIVATING              = 0x02,  ���������
CBM_ACTIVATED               = 0x04,  ������������
CBM_DEACTIVATING            = 0x08,  ����������
CBM_CSD_AUTO_DISC_TIMEOUT   = 0x10,  csd ����-��� ��������������� ����������
CBM_GPRS_AUTO_DISC_TIMEOUT  = 0x20,  gprs ��������������� ����-��� ����������
CBM_NWK_NEG_QOS_MODIFY      = 0x040, ����������� �� ��������������� QoS ���� �� ���������
*/
static char *get_StateDescription(cbm_bearer_state_enum state)
{
    switch (state)
    {
#ifdef APP_DEBUG
    DESC_DEF(CBM_DEACTIVATED);
    DESC_DEF(CBM_ACTIVATING);
    DESC_DEF(CBM_ACTIVATED);
    DESC_DEF(CBM_DEACTIVATING);
    DESC_DEF(CBM_CSD_AUTO_DISC_TIMEOUT);
    DESC_DEF(CBM_GPRS_AUTO_DISC_TIMEOUT);
    DESC_DEF(CBM_NWK_NEG_QOS_MODIFY);
    DESC_DEF(CBM_WIFI_STA_INFO_MODIFY);
#endif
    default:
    {
        static char bearer_state[10] = {0};
        snprintf(bearer_state, 10, "%d", state);
        return bearer_state;
    }
    }
}

/* ��������� ������� */

/*****************************************************************************
* ������� : ftpgettofs_final_cb
* ��������: ��������� ������� � ���������� �� �� ftp
*
* ��� �������� ������ app.bin ���������� ����� app_update()
*************************************************************************************/
ResultNotifyCb ftpgettofs_final_cb(eat_bool result)
{
    DEBUG_TRACE("ftpgettofs_cb final result = %d\r\n", result);

    if (result)
    {
        //eat_uart_write(eat_uart_app, "\r\nTEST:APP UPDATE...\r\n", 22);
        DEBUG_TRACE("\r\nTEST:APP DOWNLOAD...\r\n");
        //app_update("c:\\User\\Ftp\\app.bin");
    }
    else
    {
        //Do something
        DEBUG_TRACE("FTP download fail");
    }
}

/*****************************************************************************
* ������� : ftpgettofs_cb
* ��������: ��������� ��������� �� ftp
*****************************************************************************/
ResultNotifyCb ftpgettofs_cb(eat_bool result)
{
    u8 buffer[128] = { 0 };
    DEBUG_TRACE("ftpgettofs_cb result = %d\r\n", result);
    if (!result)
    {
        //Do something
        DEBUG_TRACE("ftpgettofs fail");
    }
}

/*****************************************************************************
* ������� : simcom_fota_update
* ��������: FOTA (Firmware Over The Air) update - ���������� ������������� ���������� ������������ �����������
* ��������� :
*     username - ��� ������������ ��� �����
*     pswd - ������ ��� ����� � �������
*     getName - ��� ����� �� �������
*     getPath - ���� � ����� �� �������
*     serv - ����� �������
*     port - ���� �������
*****************************************************************************/
eat_bool simcom_fota_update(u8 *username, u8 *pswd, u8 *getName, u8 *getPath, u8 *serv, u8 port)
{
    //eat_uart_write(eat_uart_app, "\r\nTEST:APP DOWNLOAD...\r\n", 24);
    DEBUG_TRACE("\r\nTEST:APP DOWNLOAD...\r\n");
    //simcom_ftp_down_file(username, pswd, getName, getPath, serv, port, "app.bin", ftpgettofs_cb, ftpgettofs_final_cb);
}

/*****************************************************************************
* ������� : soc_notify_cb
* ��������: ������� ��������� ������ �� soc_create()
* ��������� :
*     s_id - ������������� ������
*     event - ������� ���������� � ������� ������, ��������� � soc_event_enum
*     result - ���� ������� - SOC_CONNECT, ���� �������� ��������� � ���������� �����������
*     ack_size - ���� ������� - SOC_ACKED, ���� �������� ������������, ��� ��������� acked
*****************************************************************************/
static void soc_notify_cb(s8 s_id, soc_event_enum event, eat_bool result, u16 ack_size)
{
    s16 len = 0;
    u8  buffer[2000] = { 0 }; //or 1152 = 1K + 128 for upgrade module

    u8 val = 0;
    s8 ret = 0;
    sockaddr_struct clientAddr = { 0 };
    s8 newsocket = 0;

    static u8 connect_count = 0;
    DEBUG_TRACE("soc_notify_cb: socket_id = %d, event = %s.", s_id, get_EventDescription(event));

    switch (event)
    {
    case SOC_READ:
        while (1)
        {
            len = eat_soc_recv(s_id, buffer, sizeof(buffer));
            if (len > 0)
            {
                //Read from Server. TODO something
                DEBUG_TRACE("Recv data len=%d, %s", len, buffer);
            }
            else
            {
                DEBUG_TRACE("eat_soc_recv error: %d!", len);
                break;
            }
        }
        break;

    case SOC_CONNECT:

        if (result == TRUE)
        {
            //Connect Server Successful, TODO something
            DEBUG_TRACE("SOC_CONNECT Successful.");
            //simcom_send_data_to_server(s, "Hello", 6);
            socket_id = s_id;
            simcom_send_data_to_server(s_id, "#L#866104025710701;NA\r\n", strlen("#L#866104025710701;NA\r\n"));
        }
        else
        {
            DEBUG_TRACE("SOC_CONNECT Socket connect failed, result=%d, maybe the server is OFF.", result);
            if (connect_count < 3)
            {
                DEBUG_TRACE("�onnect again. connect_count: %d", connect_count);
                eat_soc_close(s_id);
                simcom_connect_server(setts.IPaddress);
                connect_count++;
            }
        }
        break;

    case SOC_CLOSE:
        DEBUG_TRACE("SOC_CLOSE socketid = %d", s_id);
        eat_soc_close(s_id);
        break;

    case SOC_ACKED:
        DEBUG_TRACE("SOC_NOTIFY: %d, %s, acked size of send data: %d\r\n", s_id, get_EventDescription(event), ack_size);
        break;

    case SOC_ACCEPT:
        newsocket = eat_soc_accept(s_id, &clientAddr);
        if (newsocket < 0) {
            DEBUG_TRACE("eat_soc_accept return error :%d", newsocket);
        }
        else {
            DEBUG_TRACE("client accept: %d.%d.%d.%d\r\n",
                      clientAddr.addr[0], clientAddr.addr[1], clientAddr.addr[2], clientAddr.addr[3]);
        }

        val = TRUE;
        ret = eat_soc_setsockopt(s_id, SOC_NODELAY, &val, sizeof(val));
        if (ret != SOC_SUCCESS)
            DEBUG_TRACE("eat_soc_setsockopt SOC_NODELAY return error :%d", ret);
        break;

    default:
        DEBUG_TRACE("SOC_NOTIFY s_id = %d: %s not handled. result = %d\r\n", s_id, get_EventDescription(event), result);
        break;
    }
}

/*****************************************************************************
* ������� :   hostname_notify_cb
* ��������: The callback ������� of simcom_gethostbyname
* ��������� :
*     request_id - ��������������� �� eat_soc_gethostbyname
*     result - ��������� gethostname.
*     ip_addr[4] - ���� result - TRUE, ���� �������� ��������� IP address ����� ����� hostname.
*****************************************************************************/
static void hostname_notify_cb(u32 request_id, eat_bool result, u8 ip_addr[4])
{
    if (result == TRUE)
    {
        DEBUG_TRACE("Hostname notify:%s -> %d.%d.%d.%d", setts.domain,
                  ip_addr[0], ip_addr[1], ip_addr[2], ip_addr[3], setts.port);
        //socket_connect(ip_addr); //TODO:this should be done in ������� action_hostname2ip
    }
    else
    {
        DEBUG_TRACE("hostname_notify_cb error: request_id = %d", request_id);
    }
    return;
}

/*****************************************************************************
* ������� :   simcom_gethostbyname
* ��������: This ������� gets the IP of the given domain name.
* ��������� :
*     domain_name - ������������ ������
* ����������:
*     FALSE or TRUE
*****************************************************************************/
eat_bool simcom_gethostbyname(const char *domain_name)
{
    u8 len = 0;
    u8 ip_addr[4] = {0};
    s8 result = SOC_SUCCESS;

    eat_soc_gethost_notify_register(hostname_notify_cb);

    result = eat_soc_gethostbyname(domain_name, ip_addr, &len, 1234);
    //result = eat_soc_gethostbyname(domain_name, ipaddr, &len, request_id++);

    if (result == SOC_SUCCESS) {
        DEBUG_TRACE("eat_soc_gethostbyname success");
        DEBUG_TRACE("HOSTNAME:%s -> %d.%d.%d.%d:%d.", domain_name,
                  ip_addr[0], ip_addr[1], ip_addr[2], ip_addr[3], setts.port);

        //����������� � IP ������, ����������� �� ����� ���� (������)
        return simcom_connect_server(ip_addr);
    }
    else if (result == SOC_WOULDBLOCK) {
        DEBUG_TRACE("eat_soc_gethostbyname wait callback �������.");
        return ERR_WAITING_HOSTNAME2IP;
    }
    else{
        DEBUG_TRACE("eat_soc_gethostbyname error!");
        return ERR_GET_HOSTBYNAME_FAILED;
    }
}

/*****************************************************************************
* ������� : socket_gprs_setup
* ��������: ��������� ���������� �� IP ������ ��� �� ����� ������
* ����������:
*     ��� ������ ��� ��� ��������� �����������
*****************************************************************************/
s32 socket_gprs_setup(void)
{
    DEBUG_TRACE("Setup socket GPRS.");
    if (setts.address_type == ADDRESS_TYPE_IP)
        return simcom_connect_server(setts.IPaddress);
    else
        return simcom_gethostbyname(setts.domain);
}


/*****************************************************************************
* ������� : socket_tcpip_init
* ��������: ������������� ����������� ��� �������� bearer
*****************************************************************************/
s32 socket_tcpip_init(void)
{
    //����������� ���������� �� ������ �������� GPRS
    s8 ret = eat_gprs_bearer_hold();
    DEBUG_TRACE("Hold GPRS bearer - %d", ret);
    DEBUG_TRACE("Get socket..."); //�������� �����

    //Connect Successful, TODO something.
    if (ret == CBM_OK)
    {
        DEBUG_TRACE("Hold bearer success!");
        //���������� �����������, ��������� ����������� � �������.
        DEBUG_TRACE("Connect Successful, TODO something.");

        //���������� �����������, �����������
        return socket_gprs_setup();
    }
    else
    {
        DEBUG_TRACE("Hold bearer failed!");
        return ERR_HOLD_BEARER_FAILED;
    }
    return SUCCESS;
}

/*****************************************************************************
* ������� :   bear_notify_cb
* ��������: ������� ��������� ������ �� eat_gprs_bearer_open
* ��������� :
*     state   - ������ �������� bearer
*     ip_addr - IP �����
* ����������:
*
    DESC_DEF(CBM_DEACTIVATED);
    DESC_DEF(CBM_ACTIVATING);
    DESC_DEF(CBM_ACTIVATED);
    DESC_DEF(CBM_DEACTIVATING);
    DESC_DEF(CBM_CSD_AUTO_DISC_TIMEOUT);
    DESC_DEF(CBM_GPRS_AUTO_DISC_TIMEOUT);
    DESC_DEF(CBM_NWK_NEG_QOS_MODIFY);
    DESC_DEF(CBM_WIFI_STA_INFO_MODIFY);
*****************************************************************************/
static void bear_notify_cb(cbm_bearer_state_enum state, u8 ip_addr[4])
{
    u8 buffer[128] = { 0 };
    DEBUG_TRACE("bear_notify state: %s", get_StateDescription(state));

    switch (state) //����������� ��������, ��������������� ��������� ���������� (bearer)
    {
    case CBM_ACTIVATED:
        sprintf(buffer, "BEAR_NOTIFY:%d local ip: %d.%d.%d.%d\r\n",
                state, ip_addr[0], ip_addr[1], ip_addr[2], ip_addr[3]);

        DEBUG_TRACE("Open bearer success!");
        DEBUG_TRACE("Connection socket_tcpip initialization: %d", socket_tcpip_init());
        break;

    case CBM_DEACTIVATED:
        //simcom_tcpip_gprs_start();
        break;
        //��������� �� ����� ������������
    case CBM_ACTIVATING:
        break;
    case CBM_DEACTIVATING:
        break;

    case CBM_GPRS_AUTO_DISC_TIMEOUT:
        DEBUG_TRACE("CBM_GPRS_AUTO_DISC_TIMEOUT happened!");
        eat_reset_module();
        break;

        //������ ���������
    case CBM_CSD_AUTO_DISC_TIMEOUT:
    case CBM_NWK_NEG_QOS_MODIFY:
    case CBM_WIFI_STA_INFO_MODIFY:
        break;

    default:
        sprintf(buffer, "BEAR_NOTIFY %s not handled.\r\n", get_StateDescription(state));
        break;
    }
    DEBUG_TRACE(buffer, strlen(buffer));
}

/*****************************************************************************
* ������� : simcom_connect_server
* ��������: ����������� � �������
* ��������� :
*     ip_addr -  IP ����� �������
* ����������:
*     socket id
*****************************************************************************/
s8 simcom_connect_server(u8 ip_addr[4])
{
    u8 val = TRUE;
    s8 ret = SOC_SUCCESS;
    s8 s_id;
    sockaddr_struct server_address = {SOC_SOCK_STREAM};

    eat_soc_notify_register(soc_notify_cb); //register socket callback
    s_id = eat_soc_create(SOC_SOCK_STREAM, 0);

    if (s_id < 0)
    {
        DEBUG_TRACE("eat_soc_create return error: %d", s_id);
        return ERR_SOCKET_CREATE_FAILED;
    }
    else
    {
        DEBUG_TRACE("eat_soc_create Socket is received!, s_id = %d", s_id);
    }

    val = (SOC_READ | SOC_WRITE | SOC_CLOSE | SOC_CONNECT | SOC_ACCEPT);
    ret = eat_soc_setsockopt(s_id, SOC_ASYNC, &val, sizeof(val));
    if (ret != SOC_SUCCESS)
    {
        DEBUG_TRACE("eat_soc_setsockopt set SOC_ASYNC failed: %d", ret);
        return ERR_SOCKET_OPTION_FAILED;
    }

    val = TRUE;
    ret = eat_soc_setsockopt(s_id, SOC_NBIO, &val, sizeof(val));
    if (ret != SOC_SUCCESS)
    {
        DEBUG_TRACE("eat_soc_setsockopt set SOC_NBIO failed: %d", ret);
        return ERR_SOCKET_OPTION_FAILED;
    }

    val = TRUE;
    ret = eat_soc_setsockopt(s_id, SOC_NODELAY, &val, sizeof(val));
    if (ret != SOC_SUCCESS)
    {
        DEBUG_TRACE("eat_soc_setsockopt set SOC_NODELAY failed: %d", ret);
        return ERR_SOCKET_OPTION_FAILED;
    }

    //��������� ���������� ����������� � �������
    server_address.sock_type = SOC_SOCK_STREAM;
    server_address.addr_len = 4;
    server_address.addr[0] = ip_addr[0];
    server_address.addr[1] = ip_addr[1];
    server_address.addr[2] = ip_addr[2];
    server_address.addr[3] = ip_addr[3];
    server_address.port = setts.port;                // TCP ������ ����

    DEBUG_TRACE("Connect to server with address: %d.%d.%d.%d:%d",
              server_address.addr[0], server_address.addr[1],
            server_address.addr[2], server_address.addr[3], setts.port);

    //����������� ������ � ������� � �������
    ret = eat_soc_connect(s_id, &server_address);
    if (ret >= 0) {
        DEBUG_TRACE("Socket ID of NEW connection is: %d", ret);
        return ERR_SOCKET_CONNECTED;
    }
    else if (ret == SOC_WOULDBLOCK) {
        DEBUG_TRACE("Connection is in progressing...");
        return ERR_SOCKET_WAITING;
    }
    else {
        DEBUG_TRACE("Connect return error: %d!", ret);
        return ERR_SOCKET_FAILED;
    }
}

/*****************************************************************************
* ������� : simcom_send_data_to_server
* ��������: �������� ������ �� ������, ����� ��������� ���������� ������� � eat_soc_send() � eat_socket.h
* ��������� :
*     s_id  - ������������� ������
*     buf - ����� ��� �������� ������
*     len - ������ ������
* ����������:
*     >=0 : SUCCESS - �������
*     SOC_INVALID_SOCKET : �������� ������������� ������
*     SOC_INVAL : buf ����� NULL ��� len ����� ����
*     SOC_WOULDBLOCK :  ����� �� �������� ��� bearer ���������
*     SOC_BEARER_FAIL : bearer ��������
*     SOC_NOTCONN : ����� �� ���������, � ������ � TCP
*     SOC_PIPE : ����� ��� ��� ����������
*     SOC_MSGSIZE : ��������� ������� �������
*     SOC_ERROR : ����������� ������
*     SOC_NOTBIND : � ������ �������� ������� �� ��������� ICMP, ������� ��������� ��
*****************************************************************************/
s32 simcom_send_data_to_server(s8 s_id, const void *databuf, s32 len)
{
    s32 ret = 0;
    s32 resreconn = 0;
    ret = eat_soc_send(s_id, databuf, len);
    if (ret < 0)
    {
        DEBUG_TRACE("eat_soc_send send data failed. Return error: %d!", ret);
        if (ret == SOC_PIPE || ret == SOC_NOTCONN)
        {
            DEBUG_TRACE("��� ����������� � �������. ���������������...");
            resreconn = socket_tcpip_init();

            DEBUG_TRACE("Result reconnection: %d", resreconn);
        }
    }
    else if (ret >= 0)
        DEBUG_TRACE("eat_soc_send send data to server Successful: %d", ret);

    return ret;
}

/*****************************************************************************
* ������� :   simcom_recv_from_server
* ��������: ��������� ������ �� �������, ����� ��������� ���������� ������� � eat_soc_recv() � eat_socket.h
* ��������� :
*     s_id  - socket id
*     buf - ����� ��� ������ ������
*     len - ������ ������ (�����)
* ����������:
*     0 :                   ������� FIN � �������
*     SOC_INVALID_SOCKET :  �������� ������������� ������
*     SOC_INVAL :           buf ����� NULL ��� len ����� ����
*     SOC_WOULDBLOCK :      ��� ������
*     SOC_BEARER_FAIL :     bearer ��������
*     SOC_NOTCONN :         ����� �� ���������, � ������ � TCP
*     SOC_PIPE :            ����� ��� ��� ����������
*     SOC_ERROR :           ����������� ������
*****************************************************************************/
s32 simcom_recv_from_server(s8 s_id, void *databuf, s32 len)
{
    s32 ret = 0;

    ret = eat_soc_recv(s_id, databuf, len);
    if (ret == SOC_WOULDBLOCK) {
        DEBUG_TRACE("eat_soc_recv no data available!");
    }
    else if (ret > 0) {
        DEBUG_TRACE("eat_soc_recv data: %s", databuf);
    }
    else {
        DEBUG_TRACE("eat_soc_recv return error: %d", ret);
    }
}

/*****************************************************************************
* ������� : simcom_tcpip_gprs_start
* ��������: �������� ����������� ����� �������� ����
*****************************************************************************/
void simcom_tcpip_gprs_start(void)
{
    //��������� � ��������� ������ � GPRS
    s8 bearer_ret = eat_gprs_bearer_open("CMNET", NULL, NULL, bear_notify_cb); //(u8 *apn, u8 *userName, u8 *password)

    if (bearer_ret == CBM_WOULDBLOCK)
    {
        DEBUG_TRACE("Opening bearer...");
    }
    else if (bearer_ret == CBM_OK)
    {
        DEBUG_TRACE("Open bearer success.");
    }
    else
    {
        DEBUG_TRACE("open bearer failed: bearer_ret = %d, error: %d", bearer_ret, ERR_OPEN_BEARER_FAILED);
        //return ERR_OPEN_BEARER_FAILED;
    }

    //app update entry
    /*simcom_fota_update(g_modemConfig.ftpUserName, g_modemConfig.ftpPassword,
    g_modemConfig.ftpFileName, g_modemConfig.ftpFilePath, g_modemConfig.FTPServerIP, g_modemConfig.ftpPort);*/
}

/*****************************************************************************
* ������� : socket_send_data
* ��������: �������� ����� �������� ��������� (������) �� ������
*****************************************************************************/
s32 socket_send_data(const void *databuf, s32 len)
{
    s32 ret = simcom_send_data_to_server(socket_id, databuf, len);
    return ret;
}
