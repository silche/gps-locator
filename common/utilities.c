/*---------------------------------------------------------------------
* ������������ ����� :   utilities.c
* ���� ������        :   28.02.2017
* ��������           :   ������������� ������ ������ �� �������� � ������� �����������
*---------------------------------------------------------------------*/

#include <string.h>
#include "utilities.h"
#include "minilzo.h"

/*������� ������ ���������� ��� ������.
 * �������� ������ � �������� �lzo_align_t� (������ �char�),
 * ����� ���������, ��� ��� ��������� ���������.*/

#define HEAP_ALLOC(var,size) \
    lzo_align_t __LZO_MMODEL var [ ((size) + (sizeof(lzo_align_t) - 1)) / sizeof(lzo_align_t) ]

static HEAP_ALLOC(wrkmem, LZO1X_1_MEM_COMPRESS);

//�������� ������
const unsigned char *string_trimLeft(const unsigned char *string)
{
    const unsigned char *p = string;
    while(*p == ' ') p++;
    return p;
}
void string_trimRight(unsigned char *string)
{
    unsigned char *p = string + strlen((const char *)string) - 1;
    while(*p == ' ' || *p == '\r' || *p == '\n' || *p == '\t') p--;
    *(p + 1) = 0;
    return;
}

/*
* locates the first occurrence of string s2 in the string
* and return the next pointer by s2 in s1
* if not found, the return null
*
������� ������ ��������� ������ S2 � ������
� ���������� ��������� �� ��������� �� �2 � �1,
���� �� �������, �� ������������ �������� NULL
*/

char *string_bypass(const char *s1, const char *s2)
{
    char *p = strstr(s1, s2); //������� strstr - ����� ������� ��������� ������ s2 � ������ s1
    if (p) // If contains, skip and go further
        p += strlen(s2);
    return p;
}

//equivalent to eat_acsii_to_ucs2
void ascii2unicode(unsigned short *out, const unsigned char *in)
{
    int i = 0;
    unsigned char *outp = (unsigned char*)out;
    const unsigned char *inp = in;
    while( inp[i] )
    {
        outp[i  *2] = inp[i];
        outp[i  *2 + 1] = 0x00;
        i++;
    }
    out[i] = 0;
}

void unicode2ascii(unsigned char *out, const unsigned short *in)
{
    int i = 0;
    while( in[i] )
    {
        out[i] = in[i] & 0xFF;
        i++;
    }
    out[i] = 0;
}

void Swap(char *a, char *b)
{
    char tmp = *a;
    *a = *b;
    *b = tmp;
}
char *reverse(char *str)
{
    char *begin = str;
    char *end = str;

    for (; *end; ++end) { ; }
    for (--end; begin < end; ++begin, --end)
        Swap(begin, end);
    return str;
}
//�������������� ����� double � ����� ����������
char *IntToAnsi(int dig, char str[], int len)
{
    int i = 0;
    while (dig && (i < len - 1))
    {
        str[i++] = (dig % 10) + '0';
        dig /= 10;
    }
    if ((i == 0) && (i < len))
        str[i++] = '0';

    str[i] = '\0';
    reverse(str);
    return str;
}

//�������� ������ �� ������� ������ ����
char isNumber(char *str) {
    int i = 0;
    char res = 1;
    for (i = 1; i < strlen(str) && str[i] != '\n'; ++i) {
        if (!isdigit(str[i]))
        {
            res = 0;
            break;
        }
    }
    return res;
}
