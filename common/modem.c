/*---------------------------------------------------------------------
* ������������ ����� :   modem.c
* ��������           :   ������ � ������� GSM ������
* ���� ������        :   18.06.2017
*---------------------------------------------------------------------*/
#include <string.h>
#include <stdio.h>

#include "custom_types.h"
#include <eat_network.h>

#include "modem.h"
#include "utilities.h"

#define MODEM_TEST_CMD  "=?"
#define MODEM_READ_CMD  "?"
#define MODEM_WRITE_CMD "="

#define CR  "\r"    //CR (carriage return)
#define LF  "\n"    //LF (line feed - new line)

#define AT_CGATT    "AT+CGATT"
#define AT_CENG     "AT+CENG"
#define AT_CGNSINF  "AT+CGNSINF"
#define AT_CCID     "AT+CCID"


static eat_bool modem_cmd(const unsigned char *cmd)
{
    unsigned short len = strlen(cmd);
    unsigned short rc = eat_modem_write(cmd, len);

    DEBUG_TRACE("Modem command: %s", cmd);
    if(rc != len)
    {
        DEBUG_TRACE("modem write failed: should be %d, but %d", len, rc);
        return FALSE;
    }
    else
        return TRUE;
}

eat_bool modem_IsCallReady(char *modem_rsp)
{
    char *ptr = strstr((const char *) modem_rsp, "Call Ready");
    if (ptr) return TRUE;
    return FALSE;
}

//C��������� �� � ������ ������ "SMS Ready"
eat_bool modem_IsSMSReady(char *modem_rsp)
{
    char *ptr = strstr((const char *) modem_rsp, "SMS Ready");
    if (ptr) return TRUE;
    return FALSE;
}

eat_bool modem_IsCCIDOK(char *modem_rsp)
{
    char *ptr = strstr((const char *) modem_rsp, "AT+CCID");
    if (ptr)
        return TRUE;
    return FALSE;
}

char *modem_IsUSSDrequ(char *modem_rsp)
{
    char *ussd_resp = 0;
    if(strstr(modem_rsp,"+CUSD: 0"))
        ussd_resp = string_bypass(modem_rsp, "+CUSD: 0, \"");
    else if(strstr(modem_rsp,"+CUSD: 1"))
        ussd_resp = string_bypass(modem_rsp, "+CUSD: 1,\"");
    return ussd_resp;
}

#if 0
eat_bool modem_ReadGPRSStatus(void)
{
    unsigned char *cmd = AT_CGATT MODEM_READ_CMD LF;
    return modem_cmd(cmd);
}

eat_bool modem_IsGPRSAttached(char *modem_rsp)
{
    char *ptr = strstr((const char *) modem_rsp, "+CGATT: 1");

    if (ptr)
        return TRUE;
    return FALSE;
}
#else

eat_bool modem_GPRSAttach(void)
{
    int ret = eat_network_get_creg();

    if (ret == EAT_REG_STATE_REGISTERED)
        return eat_network_get_cgatt();
    else
        DEBUG_TRACE("network register status: %d", ret);

    return FALSE;
}

#endif

/*
 * Write Command AT+CENG=<mode>[,<Ncell>]
 * Response
 * Switch on or off engineering mode. It will report +CENG: (network information) automatically if <mode>=2.
 *      OK
 *      ERROR
 *
 * Parameters
 * <mode>   0  Switch off engineering mode
 *          1  Switch on engineering mode
 *          2  Switch on engineering mode, and activate the URC report of network information
 *          3 Switch on engineering mode, with limited network information
 *          4 Switch on engineering mode, with extern information
 * <Ncell>  0 Un-display neighbor cell ID
 *          1 Display neighbor cell ID
 *  If <mode> = 3, ignore this parameter.
 *
 *  refer to AT command manual for detail
 */
eat_bool modem_switchEngineeringMode(int mode, int Ncell)
{
    unsigned char cmd[32] = {0};
    snprintf(cmd, 32, "%s%s%d,%d\r", AT_CENG, MODEM_WRITE_CMD, mode, Ncell);
    return modem_cmd(cmd);
}

eat_bool modem_readCellInfo(void)
{
    unsigned char *cmd = AT_CENG MODEM_READ_CMD CR;
    return modem_cmd(cmd);
}

//������ CCID ������
eat_bool modem_readCCIDInfo(void)
{
    unsigned char *cmd = AT_CCID CR;
    return modem_cmd(cmd);
}

//����� �� �����
eat_bool modem_Callphone_number(unsigned char *phone_number)
{
    unsigned char cmd[17] = {0};
    sprintf(cmd, "ATD+%s;\r", phone_number);
    return modem_cmd(cmd);
}

//������ ������ � ��������� ����� � ������� USSD ������
eat_bool modem_reqUSSD_number(const char *ussd_number)
{
    unsigned char cmd[160] = {0};
    sprintf(cmd, "AT+CUSD=1,\"%s\"\r", ussd_number);
    return modem_cmd(cmd);
}

eat_bool modem_cancelUSSD_session(void)
{
    unsigned char cmd[12] = {0};
    sprintf(cmd, "AT+CUSD=2\r");
    return modem_cmd(cmd);
}

eat_bool modem_AT(unsigned char *cmd)
{
    return modem_cmd(cmd);
}

