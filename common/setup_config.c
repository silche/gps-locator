/*---------------------------------------------------------------------
* ������������ ����� :   setup_config.c
* ���� ������        :   24.02.2017
* ��������           :   ��������� ������ ������� �������
*---------------------------------------------------------------------*/
#include "setup_config.h"

/* ��������� ������� */

//��������� ������ GPS ������
eat_bool gps_module_init(void)
{
    unsigned short len = 0;
    u8 buf[] = {0xA0,0xA1,0x00,0x09,MSG_ID,GGA,GSA,GSV,GLL,RMC,VTG,ZDA,ATTR,
        0x09, //��� �����, ������� �� ����� msg body (������� ^)
        0x0D,0x0A};

    if(len = eat_uart_write(eat_uart_app, buf, sizeof(buf)) > 0)
    {
        DEBUG_TRACE("GPS-������ ��������. �������� %d ����.",len);
        return TRUE;
    }
    else
        return FALSE;
}

//������������� ����� ����
eat_bool uart_initialize(void)
{
    eat_bool result = FALSE;
    EatUartConfig_st uart_config;
    if(eat_uart_open(eat_uart_app ) == FALSE)  //�������� ����� uart
        DEBUG_TRACE("[%s] uart(%d) open fail!", __FUNCTION__, eat_uart_app);
    else
    {
        if (EAT_UART_USB != eat_uart_app)//USB ���� �� ��������� � ��������� ������������
        {//��������� ����������������� �����
            uart_config.baud = EAT_UART_BAUD_9600; //��������
            uart_config.dataBits = EAT_UART_DATA_BITS_8; //����� ����� � �����
            uart_config.parity = EAT_UART_PARITY_NONE; // �/��� ���� ��������
            uart_config.stopBits = EAT_UART_STOP_BITS_1; // �������� ���

            if(TRUE == eat_uart_set_config(eat_uart_app, &uart_config))
                result = TRUE;
            else
                DEBUG_TRACE("[%s] uart(%d) set config fail!", __FUNCTION__, eat_uart_app);

            eat_uart_set_send_complete_event(eat_uart_app, TRUE);
        }
    }
    return result;
}
