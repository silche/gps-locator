/*---------------------------------------------------------------------
* ������������ ����� :   settings.�
* ��������           :   ������/���������� ��������/���������� ����������
* ���� ������        :   22.02.2017
*---------------------------------------------------------------------*/

/* ������������ ����� */
#include <string.h>
#include <stdio.h>

#include <eat_modem.h>

#include "settings.h"
#include "file_system.h"
#include "cJSON.h"
#include "utilities.h"

/* ������� */
//for debug command
#define CMD_STRING_SERVER   "server"

//for JSON tag
#define TAG_USER  "USER"
#define TAG_PHONE_NUMBER   "PHONE_NUMBER"
#define TAG_PASSWORD   "PASSWORD"
#define TAG_UTC    "UTC"
#define TAG_DEVNAME  "DEVNAME"
#define TAG_BAL_NUM  "BALANCE_NUMBER"

#define TAG_SERVER  "SERVER"
#define TAG_ADDRESS_TYPE   "ADDRESS_TYPE"
#define TAG_ADDRESS   "ADDRESS"
#define TAG_PORT    "PORT"

#define TAG_TIMERS  "TIMERS"
#define TAG_SLEEP    "SLEEP"
#define TAG_GPS_SEND    "GPS_SEND"
#define TAG_MAIN_APP  "MAIN_APP"

/* ���� */
typedef struct
{
    //Server configuration
    ADDRESS_TYPE addr_type;
    union
    {
        char domain[MAX_DOMAIN_NAME_LEN];
        u8 IPaddr[4];
    }addr;
    u16 port;

    //Timer configuration
    u32 gps_send_timer_period;

}STORAGE;

SETTINGS setts;

/* ��������� ������� */

//������������� ���������� �������� ��� ������ ��������� ����������
void Initial_Settings(void)
{
    cJSON_Hooks mem_hooks;

    mem_hooks.malloc_fn = malloc;
    mem_hooks.free_fn = free;

    DEBUG_TRACE("������������� �������� �� ���������.");

    cJSON_InitHooks(&mem_hooks);//������������� cJSON ������

    /* Server configuration */
//#if 1
    setts.address_type = ADDRESS_TYPE_IP;

    //193.193.165.166 - IP address server gps-trace orange
    setts.IPaddress[0] = 193;
    setts.IPaddress[1] = 193;
    setts.IPaddress[2] = 165;
    setts.IPaddress[3] = 166;
//#else
//    settings.address_type = ADDRESS_TYPE_DOMAIN;
//    strncpy(settings.domain, "www.gps-trace.com",MAX_DOMAIN_NAME_LEN);
//#endif
    setts.port = 20332;//server port - Wialon IPS

    /* User configuration */
    strcpy(setts.device_name, "����");
    strcpy(setts.sms_Password, "1234");
    setts.utc = 0;

    /* Timer configuration */
    setts.sleep_timer_period = 7 * 24 * 60 * 60 * 1000; //���������� � ������ ��� � ������� ������
    setts.timeupdate_timer_peroid = 24 * 60 * 60 * 1000;      //24h * 60m * 60s * 1000ms
    setts.gps_send_period = 20 * 1000; //�������� ��������� ��� � 20 ������
    setts.main_app_timer_period = 30 * 60 * 1000; //����� ������ ��������� 30 �����

    return;
}
//���������� ����������� ���������� � ����

eat_bool Save_Settings(void)
{
    cJSON *root = CreateObject_cJSON();
    cJSON *user = CreateObject_cJSON();
    cJSON *timers = CreateObject_cJSON();
    //cJSON *mode = CreateObject_cJSON(); //����� ������
    //cJSON *address = CreateObject_cJSON();

    char *content = 0;

    //���������� �������� � JSON ���������

    //���������� ���������������� ����������
    cJ_AddStringToObject(user, TAG_DEVNAME, setts.device_name);
    cJ_AddStringToObject(user, TAG_PASSWORD, setts.sms_Password);
    cJ_AddStringToObject(user, TAG_PHONE_NUMBER, setts.phone_Number);
    cJ_AddStringToObject(user, TAG_BAL_NUM, setts.balance_number);
    cJ_AddItemToObject(root, TAG_USER, user);

    //���������� �������� �������� � �������� �����
    cJ_AddNumberToObject(timers, TAG_MAIN_APP, setts.main_app_timer_period);
    cJ_AddNumberToObject(timers, TAG_GPS_SEND, setts.gps_send_period);
    cJ_AddNumberToObject(timers, TAG_SLEEP, setts.sleep_timer_period);
    cJ_AddNumberToObject(timers, TAG_UTC, setts.utc);
    cJ_AddItemToObject(root, TAG_TIMERS, timers);

    content = cJ_PrintUnformatted(root); //�������������� ��������� ��������� � ��������� ������
    DEBUG_TRACE("���������� ����������� ���������� ��������...");

    //������ �������� � ���������������� ����
    fs_write_to_file(content);
    free(content);
    cJSON_Delete(root);
}

//�������������� ����� ���������� � ���� ��������
eat_bool Restore_Settings(void)
{
    char *readBuf = 0;
    cJSON *conf = 0, *user = 0, *timers = 0, *mode = 0, *address = 0;

    Initial_Settings(); //���������� �������������� ����������� ���������

    DEBUG_TRACE("�������������� �������� �� ����� ������������ �����...");
    readBuf = fs_open_read_file();
    if(readBuf == 0)
        return FALSE;

    //������ (parse) JSON ������
    conf = cJ_Parse(readBuf);
    if (!conf)
    {
        //DEBUG_TRACE("setting config file format error!");
        DEBUG_TRACE("������ ������� �������� � �����!");
        //eat_fs_Close(file_handle);
        free(readBuf);
        cJSON_Delete(conf);
        return FALSE;
    }

    //�������������� ���������������� ��������
    user = cJ_GetObjectItem(conf, TAG_USER);
    if (!user)
    {
        //DEBUG_TRACE("no user config in setting file!");
        DEBUG_TRACE("������������ USER �� ���������� � ����� ��������!");
        //eat_fs_Close(file_handle);
        free(readBuf);
        cJSON_Delete(conf);
        return FALSE;
    }

    strcpy(setts.device_name, cJ_GetObjectItem(user, TAG_DEVNAME)->valuestring);
    strcpy(setts.sms_Password, cJ_GetObjectItem(user, TAG_PASSWORD)->valuestring);
    strcpy(setts.phone_Number, cJ_GetObjectItem(user, TAG_PHONE_NUMBER)->valuestring);
    strcpy(setts.balance_number, cJ_GetObjectItem(user, TAG_BAL_NUM)->valuestring);

    //�������������� �������� ��������
    timers = cJ_GetObjectItem(conf, TAG_TIMERS);
    if (!timers)
    {
        //DEBUG_TRACE("no user config in setting file!");
        DEBUG_TRACE("������������ TIMERS �� ���������� � ����� ��������!");
        //eat_fs_Close(file_handle);
        free(readBuf);
        cJSON_Delete(conf);
        return FALSE;
    }

    setts.main_app_timer_period = cJ_GetObjectItem(timers, TAG_MAIN_APP)->valueint;
    setts.gps_send_period = cJ_GetObjectItem(timers, TAG_GPS_SEND)->valueint;
    setts.sleep_timer_period = cJ_GetObjectItem(timers, TAG_SLEEP)->valueint;
    setts.utc = cJ_GetObjectItem(timers, TAG_UTC)->valueint;

    free(readBuf);
    //eat_fs_Close(file_handle);
    cJSON_Delete(conf);

    return TRUE;
}
