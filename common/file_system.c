/*---------------------------------------------------------------------
* ������������ ����� :   system.c
* ��������           :   ������ � ������ ������ � ����-������ ����������
* ���� ������        :   22.02.2017
*---------------------------------------------------------------------*/

/* ������������ ����� */
#include <eat_fs.h>
#include <eat_fs_type.h>
#include <eat_fs_errcode.h>

#include "custom_types.h"
#include "file_system.h"
#include "utilities.h"

/* ��������� ���������� (STATIC) */
static eat_fs_error_enum fs_oper_ret;
static char *readBuf = 0;
static FS_HANDLE file_Handle, seekRet;
static UINT writedLen,filelen,readLen,filesize;

/* ��������� ������� */

//��������� ������� (�����) �����
static eat_bool fs_get_file_size(void)
{
    filesize = 0;
    fs_oper_ret = (eat_fs_error_enum)eat_fs_GetFileSize(file_Handle, &filesize);
    if(EAT_FS_NO_ERROR == fs_oper_ret)
    {
        DEBUG_TRACE("eat_fs_GetFileSize(): ������� ������ �����: size id %d", filesize);
        return TRUE;
    }
    else
    {
        DEBUG_TRACE("eat_fs_GetFileSize(): ������ ��������� ������� �����, Error: %d", fs_oper_ret);
        eat_fs_Close(file_Handle);
        return FALSE;
    }
}

//�������� ����� � ������ ������ �� ���� � �����
char *fs_open_read_file(void)
{
    //�������� �����
    file_Handle = eat_fs_Open(SETTINGS_FILE_PATHNAME, FS_READ_ONLY); //��������� ���� ��� ������
    if(file_Handle == EAT_FS_FILE_NOT_FOUND)
    {
        DEBUG_TRACE("���� �� ���������� � �������!"); //not exists
        return 0;
    }
    if (file_Handle < EAT_FS_NO_ERROR) //���� ���� ������ �������� �����
    {
        DEBUG_TRACE("������ ������ �����, rc: %d", file_Handle);
        return 0;
    }

    //���� ��������� �����
    seekRet = eat_fs_Seek(file_Handle,0,EAT_FS_FILE_BEGIN);
    if(0 > seekRet)
    {
        DEBUG_TRACE("��������� �� ���� �� ������.\n");
        eat_fs_Close(file_Handle);
        return 0;
    }
    else
        DEBUG_TRACE("����� ��������� �� ���� - Success");

    //��������� ������� �����
    if(fs_get_file_size())
        readBuf = malloc(filesize); //��������� ������ �� ������ �����
    if (!readBuf)
    {
        DEBUG_TRACE("��������� ������ (malloc) ��� content ������ �� ����� - failed");
        eat_fs_Close(file_Handle);
        return 0;
    }
    else
        DEBUG_TRACE("��������� (malloc) %d bytes �� ������ ������ �� �����", filesize);

    //������ ������ �� �����
    fs_oper_ret = (eat_fs_error_enum)eat_fs_Read(file_Handle, readBuf, filesize, &readLen);
    if (fs_oper_ret == EAT_FS_NO_ERROR)
    {
        DEBUG_TRACE("������ ����� - Success");
        DEBUG_TRACE("����������� ������ (readBuf): %s", readBuf);
    }
    else
    {
        DEBUG_TRACE("������ ����� - Fail. Error: %d, Readlen: %d", fs_oper_ret, readLen);
        eat_fs_Close(file_Handle);
        free(readBuf);
        return 0;
    }
    eat_fs_Close(file_Handle); //�������� ������������ �����
    return readBuf;
}

//������ ������ � ����, ������������� � �������� ������� ����������
eat_bool fs_write_to_file(u8 content_buf[])
{
    eat_bool ret_res = FALSE;
    //test write 2K chars to file    //memset(writeBuf,0x00,2048);

    //�������� �����, ���� �������� ��� �� ������ � ������
    file_Handle = eat_fs_Open(SETTINGS_FILE_PATHNAME, FS_CREATE|FS_READ_WRITE);
    if(EAT_FS_NO_ERROR <= file_Handle)
    {
        DEBUG_TRACE("Open file - success, file handle=%d.", file_Handle);

        //������ ������ � ������� � ����
        fs_oper_ret = (eat_fs_error_enum)eat_fs_Write(file_Handle, content_buf, strlen(content_buf), &writedLen);
        if(EAT_FS_NO_ERROR == fs_oper_ret || strlen(content_buf) == writedLen)
        {
            DEBUG_TRACE("������ � ���� - success!");
            DEBUG_TRACE("writeBuf: %s\n", content_buf);
            ret_res = TRUE;
        }
        else
            DEBUG_TRACE("������ � ���� - failed. Error: %d", fs_oper_ret);
    }
    else
        DEBUG_TRACE("Open file - failed, file handle=%d.", file_Handle);
    eat_fs_Close(file_Handle);
    return ret_res;
}
